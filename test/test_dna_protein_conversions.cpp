//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#include <dna_protein_conversions.h>
#include <UnitTest++/UnitTest++.h>

#include <algorithm>

SUITE(DnaProteinConversions)
{
    TEST(getAAPos)
    {
        //forward checks
        CHECK_EQUAL(3, getAAPos(100, 10, 0, false));
        CHECK_EQUAL(3, getAAPos(100, 10, 1, false));
        CHECK_EQUAL(2, getAAPos(100, 10, 2, false));

        CHECK_EQUAL(3, getAAPos(100, 11, 0, false));
        CHECK_EQUAL(3, getAAPos(100, 11, 1, false));
        CHECK_EQUAL(3, getAAPos(100, 11, 2, false));

        CHECK_EQUAL(4, getAAPos(100, 12, 0, false));
        CHECK_EQUAL(3, getAAPos(100, 12, 1, false));
        CHECK_EQUAL(3, getAAPos(100, 12, 2, false));

        CHECK_EQUAL(-1, getAAPos(100, 100, 0, false)); //out of bounds right
        CHECK_EQUAL(-1, getAAPos(100, 0, 1, false)); //out of bounds left
        CHECK_EQUAL(-1, getAAPos(100, 1, 2, false)); //out of bounds left

        //reverse checks
        CHECK_EQUAL(29, getAAPos(100, 12, 0, true));
        CHECK_EQUAL(28, getAAPos(100, 12, 1, true));
        CHECK_EQUAL(28, getAAPos(100, 12, 2, true));

        CHECK_EQUAL(29, getAAPos(100, 11, 0, true));
        CHECK_EQUAL(29, getAAPos(100, 11, 1, true));
        CHECK_EQUAL(28, getAAPos(100, 11, 2, true));

        CHECK_EQUAL(29, getAAPos(100, 10, 0, true));
        CHECK_EQUAL(29, getAAPos(100, 10, 1, true));
        CHECK_EQUAL(29, getAAPos(100, 10, 2, true));

        CHECK_EQUAL(-1, getAAPos(100, 100, 0, true)); //out of bounds right
        CHECK_EQUAL(-1, getAAPos(100, 100, 1, true)); //out of bounds left
        CHECK_EQUAL(-1, getAAPos(100, 99, 2, true)); //out of bounds left
    }

    TEST(getCodonPos)
    {
        //forward checks
        CHECK_EQUAL(9, getCodonPos(100, 9, 0, false)); //codons are [6,8][9,11]
        CHECK_EQUAL(9, getCodonPos(100, 10, 0, false));
        CHECK_EQUAL(9, getCodonPos(100, 11, 0, false));

        CHECK_EQUAL(7, getCodonPos(100, 9, 1, false)); //now codons are [7,9][10,12]
        CHECK_EQUAL(10, getCodonPos(100, 10, 1, false));
        CHECK_EQUAL(10, getCodonPos(100, 11, 1, false));

        CHECK_EQUAL(8, getCodonPos(100, 9, 2, false)); //now codons are [8,10][11,13]
        CHECK_EQUAL(8, getCodonPos(100, 10, 2, false));
        CHECK_EQUAL(11, getCodonPos(100, 11, 2, false));

        CHECK_EQUAL(96, getCodonPos(100, 96, 0, false)); //codons are [96,98]
        CHECK_EQUAL(96, getCodonPos(100, 97, 0, false));
        CHECK_EQUAL(96, getCodonPos(100, 98, 0, false));

        CHECK_EQUAL(-1, getCodonPos(100, 100, 0, false)); //out of bounds right
        CHECK_EQUAL(-1, getCodonPos(100, 99, 0, false)); // codon is [96,98][99,101]
        CHECK_EQUAL(-1, getCodonPos(100, 98, 2, false)); //codon  is[98,100]

        //reverse checks
        CHECK_EQUAL(10, getCodonPos(100, 12, 0, true)); //codons are [7,9][10,12]
        CHECK_EQUAL(10, getCodonPos(100, 11, 0, true));
        CHECK_EQUAL(10, getCodonPos(100, 10, 0, true));

        CHECK_EQUAL(12, getCodonPos(100, 12, 1, true)); //codons are [6,8][9,11][12,14]
        CHECK_EQUAL(9, getCodonPos(100, 11, 1, true));
        CHECK_EQUAL(9, getCodonPos(100, 10, 1, true));

        CHECK_EQUAL(11, getCodonPos(100, 12, 2, true)); //codons are [5,7][8,10][11,13]
        CHECK_EQUAL(11, getCodonPos(100, 11, 2, true));
        CHECK_EQUAL(8, getCodonPos(100, 10, 2, true));

        CHECK_EQUAL(-1, getCodonPos(100, 100, 0, true)); //out of bounds right
        CHECK_EQUAL(97, getCodonPos(100, 99, 0, true)); // first codon [97,99]
        CHECK_EQUAL(-1, getCodonPos(100, 99, 1, true)); // before first codon [96,98]
        CHECK_EQUAL(-1, getCodonPos(100, 98, 2, true)); // before first codon [95,97]
        CHECK_EQUAL(0, getCodonPos(100, 0, 1, true)); // last codon [0,2]
        CHECK_EQUAL(-1, getCodonPos(100, 0, 0, true)); // before last codon [1,3]
        CHECK_EQUAL(-1, getCodonPos(100, 1, 2, true)); // before last codon [2,4]
    }

    TEST(getCodonPosFromAAPos)
    {
        //forward checks
        CHECK_EQUAL(3, getCodonPosFromAAPos(100, 1, 0, false)); //codons are [3,5]
        CHECK_EQUAL(30, getCodonPosFromAAPos(100, 10, 0, false)); //codons are [30,32]
        CHECK_EQUAL(31, getCodonPosFromAAPos(100, 10, 1, false)); //codons are [31,33]
        CHECK_EQUAL(32, getCodonPosFromAAPos(100, 10, 2, false)); //codons are [32,34]

        CHECK_EQUAL(-1, getCodonPosFromAAPos(100, 33, 0, false)); //out of bounds -> codon [99,101]
        CHECK_EQUAL(97, getCodonPosFromAAPos(100, 32, 1, false)); //last codon [97,99]
        CHECK_EQUAL(-1, getCodonPosFromAAPos(100, 32, 2, false)); //out of bounds -> codon [98,100]

        //reverse checks
        CHECK_EQUAL(94, getCodonPosFromAAPos(100, 1, 0, true)); //codons are [94,96]
        CHECK_EQUAL(93, getCodonPosFromAAPos(100, 1, 1, true)); //codons are [93,95]
        CHECK_EQUAL(92, getCodonPosFromAAPos(100, 1, 2, true)); //codons are [92,94]

        CHECK_EQUAL(-1, getCodonPosFromAAPos(100, 33, 0, true)); //out of bounds -> codon [-2,0]
        CHECK_EQUAL(1, getCodonPosFromAAPos(100, 32, 0, true)); //last codon [1,3]
        CHECK_EQUAL(0, getCodonPosFromAAPos(100, 32, 1, true)); //last codon [0,2]
        CHECK_EQUAL(-1, getCodonPosFromAAPos(100, 32, 2, true)); //out of bounds -> codon [-1,1]
    }
}

int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}






