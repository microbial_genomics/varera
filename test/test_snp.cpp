//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#include <snp_file.h>
#include <contig_loading.h>
#include <options.h>

#include <UnitTest++/UnitTest++.h>

#include <algorithm>


#define XSTR(x) STR(x)
#define STR(x) #x

typedef seqan::Pair<unsigned, unsigned> TLocusAlleleId;
class Data
{
public:
    Data()
    {
#ifdef SOURCE_DIR
        std::string sourceDir = XSTR(SOURCE_DIR);
        sourceDir.erase(std::remove(sourceDir.begin(), sourceDir.end(), '\"'), sourceDir.end());
#else
        std::cerr << "Missing source directory variable -- terminate test\n";
#endif
        opts.snpFile = sourceDir + "/data/tests/snps.txt";
        opts.alleleFileName = sourceDir + "/data/tests/nucleotide_fasta_protein_variant_model.fasta";
        numClusters = loadContigData(fragStore, contigInfos, contigTypes, contigToLocus, opts);
        readSnpFile(knownSnps, fragStore.contigNameStore, opts.snpFile);
        numSnps = knownSnps.knownSnps.size();
    }

    //Vars
    Options opts;
    TFragmentStoreContigs fragStore;
    seqan::String<TLocusAlleleId> contigToLocus;
    TContigTypeList contigTypes;
    KnownSnps<CardSnpRecord> knownSnps;
    seqan::String<ContigGeneInfo> contigInfos;
    size_t numSnps, numClusters;
};

Data d;

SUITE(ParsingSnpFile)
{
//    TEST(NumParsed)
//    {

//#define XSTR(x) STR(x)
//#define STR(x) #x
//#ifdef SOURCE_DIR
//        std::string sourceDir = XSTR(SOURCE_DIR);
//        sourceDir.erase(std::remove(sourceDir.begin(), sourceDir.end(), '\"'), sourceDir.end());
//#else
//        std::cerr << "Missing source directory variable -- terminate test\n";
//#endif
//        typedef seqan::Pair<unsigned, unsigned> TLocusAlleleId;
//        Options opts;
//        std::cerr << "sourceDir: " << sourceDir << '\n';
//        opts.snpFile = sourceDir + "/data/tests/snps.txt";
//        opts.alleleFileName = sourceDir + "/data/tests/nucleotide_fasta_protein_variant_model.fasta";

//        TFragmentStoreContigs fragStore;
//        seqan::String<TLocusAlleleId> contigToLocus;
//        TContigTypeList contigTypes;
//        KnownSnps<CardSnpRecord> knownSnps;
//        seqan::String<ContigGeneInfo> contigInfos;

//        Data d;

//        unsigned numClusters = loadContigData(fragStore, contigInfos, contigTypes, contigToLocus, opts);

    TEST(NumContigs)
    {
        CHECK_EQUAL(140, length(d.fragStore.contigNameStore));
    }

    TEST(NumClusters)
    {
        CHECK_EQUAL(140, d.numClusters);
    }

    TEST(NumSnps)
    {
//        size_t numSnps = readSnpFile(knownSnps, fragStore.contigNameStore, opts.snpFile);
        CHECK_EQUAL(1051, d.numSnps);
    }

    TEST(SnpsToId)
    {
        unsigned contigId = 19;
        KnownSnps<CardSnpRecord>::TSnpIter beginIt, endIt;
        size_t numSnpsForId = d.knownSnps.getSnpsForContig(beginIt, endIt, contigId);

        CHECK_EQUAL(120, numSnpsForId);

        //contig 19 should have accession 3003394
        CHECK_EQUAL(3003394, beginIt->second.accession);

        //first entry:
        //3003394 "Mycobacterium tuberculosis pncA mutations conferring resistance to pyrazinamide"       "protein variant model" "single resistance variant"     T168N
        CHECK_EQUAL(std::string("Mycobacterium tuberculosis pncA mutations conferring resistance to pyrazinamide"), std::string(seqan::toCString(beginIt->second.name)));
        CHECK_EQUAL("protein variant model", beginIt->second.modelType);
        CHECK_EQUAL(CardSnpRecord::SINGLE_RESISTANCE_VARIANT, beginIt->second.paramType);
        CHECK_EQUAL('Y', beginIt->second.mutations[0].from);
        CHECK_EQUAL('S', beginIt->second.mutations[0].to);
        CHECK_EQUAL(33, beginIt->second.mutations[0].pos);

        //entry 23
        for(size_t k = 0; k < 22; ++k, ++beginIt);
        CHECK_EQUAL(std::string("Mycobacterium tuberculosis pncA mutations conferring resistance to pyrazinamide"), std::string(seqan::toCString(beginIt->second.name)));
        CHECK_EQUAL("protein variant model", beginIt->second.modelType);
        CHECK_EQUAL(CardSnpRecord::NONSENSE_MUTATION, beginIt->second.paramType);
        CHECK_EQUAL('Y', beginIt->second.mutations[0].from);
        CHECK_EQUAL('*', beginIt->second.mutations[0].to);
        CHECK_EQUAL(98, beginIt->second.mutations[0].pos);

        //entry 64
        for(size_t k = 0; k < 41; ++k, ++beginIt);
        CHECK_EQUAL(std::string("Mycobacterium tuberculosis pncA mutations conferring resistance to pyrazinamide"), std::string(seqan::toCString(beginIt->second.name)));
        CHECK_EQUAL("protein variant model", beginIt->second.modelType);
        CHECK_EQUAL(CardSnpRecord::MULTIPLE_RESISTANCE_VARIANTS, beginIt->second.paramType);
        CHECK_EQUAL(2, length(beginIt->second.mutations));
        CHECK_EQUAL('Q', beginIt->second.mutations[0].from);
        CHECK_EQUAL('P', beginIt->second.mutations[0].to);
        CHECK_EQUAL(9, beginIt->second.mutations[0].pos);
        CHECK_EQUAL('Y', beginIt->second.mutations[1].from);
        CHECK_EQUAL('D', beginIt->second.mutations[1].to);
        CHECK_EQUAL(98, beginIt->second.mutations[1].pos);

        //check consistency between parsed (and translated) sequences and mutations in snp file
        size_t correctAA = 0, incorrecAA = 0;
        std::set<size_t> invalidAccs, totalAccs;
        for (size_t i = 0; i < length(d.fragStore.contigStore); ++i)
        {
            if (d.contigInfos[i].frame == static_cast<decltype(d.contigInfos[i].frame)>(-1))
                continue;

            totalAccs.insert(i);
            numSnpsForId = d.knownSnps.getSnpsForContig(beginIt, endIt, i);

            while(beginIt != endIt)
            {
                for (const auto & m : beginIt->second.mutations)
                {
                    seqan::AminoAcid transAA = getAAAtPos(d.fragStore.contigStore[i].seq, m.pos, d.contigInfos[i].frame, d.contigInfos[i].reversed);
                    CHECK_EQUAL(m.from, transAA);
                    if(m.from == transAA)
                    {
                        ++correctAA;
                    }
                    else
                    {
                        invalidAccs.insert(i);
                        ++incorrecAA;
                        std::cerr << "INCORRECT AA!! " << d.fragStore.contigNameStore[i] << '\t' << d.contigInfos[i].frame << '\t' << d.contigInfos[i].reversed << '\n';
                        std::cerr << m.pos << '\n';
                    }
                }
                ++beginIt;
            }
        }
        std::cerr << "correct AA " << correctAA << "\tincorrectAA: " << incorrecAA << "\tvalid Accs: " << totalAccs.size() - invalidAccs.size() << "\tinvalid Accs: " << invalidAccs.size() << '\n';
    }

    TEST(evaluateMutations)
    {
        //test evaluateMutations
        size_t cId = 35;
        SingleAlleleStats allele;
        allele.contigId = cId;
        seqan::String<MutationDNA> mutationsDNA;

        KnownSnps<CardSnpRecord>::TSnpIter it1, it2;
//        std::cout << fragStore.contigNameStore[cId] << '\t' << contigInfos[cId].reversed << '\t' << contigInfos[cId].frame
//                                                   << '\t' << knownSnps.getSnpsForContig(it1, it2, 77) << '\n';
        int conf1 = evaluateMutations(allele, mutationsDNA, d.fragStore.contigStore[cId].seq, d.contigInfos[cId], d.knownSnps);
        CHECK_EQUAL(0, conf1);

        //test single mutation
        MutationDNA m1 = {MutationDNA::MISMATCH, 'G', 'A', 358}; //mutation from GGC -> GAC (G120D)
        seqan::appendValue(mutationsDNA, m1);
        int conf2 = evaluateMutations(allele, mutationsDNA, d.fragStore.contigStore[cId].seq, d.contigInfos[cId], d.knownSnps);
        CHECK_EQUAL(1, conf2);

        //test multiple mutation
        seqan::clear(mutationsDNA);
        MutationDNA m2a1 = {MutationDNA::MISMATCH, 'G', 'C', 357}; //mutation from GGC -> CGC (G120P)
        seqan::appendValue(mutationsDNA, m2a1);
        int conf3 = evaluateMutations(allele, mutationsDNA, d.fragStore.contigStore[cId].seq, d.contigInfos[cId], d.knownSnps);
        CHECK_EQUAL(0, conf3);
        MutationDNA m2a2 = {MutationDNA::MISMATCH, 'G', 'C', 358}; //mutation from CGC -> CCC (G120P)
        seqan::appendValue(mutationsDNA, m2a2);
        int conf4 = evaluateMutations(allele, mutationsDNA, d.fragStore.contigStore[cId].seq, d.contigInfos[cId], d.knownSnps);
        CHECK_EQUAL(0, conf4);
        MutationDNA m2b = {MutationDNA::MISMATCH, 'G', 'C', 360}; //mutation from GCC -> CCC (A121P)
        seqan::appendValue(mutationsDNA, m2b);
        int conf5 = evaluateMutations(allele, mutationsDNA, d.fragStore.contigStore[cId].seq, d.contigInfos[cId], d.knownSnps);
        CHECK_EQUAL(1, conf5);
    }
}

int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}


//TEST_CASE( "Parsing snp file", "[readSnpFile]" )
//{
//    typedef seqan::Pair<unsigned, unsigned> TLocusAlleleId;
//    Options opts;
//    opts.snpFile = "./data/test/snps.txt";
//    opts.alleleFileName = "./data/test/nucleotide_fasta_protein_variant_model.fasta";

//    TFragmentStoreContigs fragStore;
//    seqan::String<TLocusAlleleId> contigToLocus;
//    TContigTypeList contigTypes;
//    KnownSnps<CardSnpRecord> knownSnps;

//    unsigned numClusters = loadContigData(fragStore, contigTypes, contigToLocus, opts);
//    readSnpFile(knownSnps, fragStore.contigNameStore, opts.snpFile);

//    REQUIRE(knownSnps.knownSnps.size() == 1131);
//    //get contigID for accession 3003394
//    unsigned contigId = -1;
//    auto iters = knownSnps.knownSnps.equal_range(contigId);
//    REQUIRE(knownSnps.knownSnps.count(contigId) == 120);


//}





