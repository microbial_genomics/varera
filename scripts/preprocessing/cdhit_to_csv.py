'''
Created on 2017
@author: Sandro Andreotti (Based on script by Harriet Dashnow, Kat Holt)
Output a tab-separated table with each row representing one gene
    <cluster id> <cluster elem nr.> <gene name> with clusters accoring to the cd-hit output.
'''

import sys
from argparse import ArgumentParser


def parse_args():
	parser = ArgumentParser(description='Parse cdhit results')

	parser.add_argument('--cluster_file',
						required = True,
						help = 'cd hit output file (.clstr)')
	parser.add_argument('--infasta_file',
						required = True,
						help = 'raw sequences file (fasta)')
	parser.add_argument('--outfile',
						required = True,
						help = 'output file (csv)')
	return parser.parse_args()

def main():
	args = parse_args()
	clusters_cd_hit = {}
	clusters_out = {}
	with open(args.outfile,"w") as outfile:
		for line in open(args.cluster_file):
			if line.startswith(">"):
				cluster = int(line.split()[1])
				continue
 
			line_split =  line.split()
			gene_prefix = line_split[2].strip("*.>")
			if cluster not in clusters_out:
				clusters_out[cluster] = []
			clusters_out[cluster].append(gene_prefix)

		for id, genes in clusters_out.items():
			gene_id = 1
			for gene in genes:
				outfile.write(' '.join([str(id), str(gene_id), gene, '\n']))
				print(id, gene_id, gene)
				gene_id += 1
	
if __name__ == '__main__':
	sys.exit(main())
