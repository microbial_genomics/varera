//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#ifndef CONTIG_LOADING_H
#define CONTIG_LOADING_H

#include <options.h>
#include <mapcounting.h>
#include <exception>
#include <regex>

const std::string WHITESPACE = " \n\r\t\f\v";
std::string trim(const std::string& s)
{
    size_t end = s.find_last_not_of(WHITESPACE);
    return (end == std::string::npos) ? "" : s.substr(0, end + 1);
}

//parse fasta header id string and check whether format follows one of the known formatting rules
//edit this function by adding an additiona regex and extraction for new formats
template<typename TContigDetails>
FastaHeaderType parseFastaHeader(TContigDetails & contigDetails,
                                 const std::string & line)
{
    std::string lineTr = trim(line);
    //check CARD format "gb|GQ343019|+|132-1023|ARO:3002999|CblA-1 [mixed culture bacterium AX_gF3SD01_15]"
    //the id parsed by seqan stops at first blank -> gb|GQ343019|+|132-1023|ARO:3002999|CblA-1
    std::regex regexCard(">gb\\|(.+)\\|[+-]\\|(.+)\\|(ARO:[[:digit:]]+)\\|(.+) (\\[.+\\])");

    //check resfinder and virulence finder format ">fosB1_1_CP001903"
    std::regex regexResFinder(">(.+)_([[:digit:]]+)_(.+)");

    //check AMR header
    std::regex regexAMR(">([[:digit:]]+)_(.+)\\|\\[gene=(.+)\\]");

    std::smatch baseMatch;
    ContigHeaderDetail cd;
    if (std::regex_match(lineTr, baseMatch, regexCard))
    {
        if (baseMatch.size() == 6 && baseMatch[1].length() != 0 && baseMatch[4].length() != 0)
        {
            cd.full = lineTr.substr(1);
            std::ssub_match subMatch = baseMatch[4];
            cd.shortId = subMatch.str();
            subMatch = baseMatch[1];
            cd.shortId += "_" + subMatch.str();
            seqan::appendValue(contigDetails, cd);
            return FastaHeaderType::CARD;
        }
    }
    else if (std::regex_match(lineTr, baseMatch, regexResFinder))
    {
        if (baseMatch.size() == 4 && baseMatch[1].length() != 0 && baseMatch[2].length() != 0)
        {
            cd.full = lineTr.substr(1);
            std::ssub_match subMatch = baseMatch[1];
            cd.shortId = subMatch.str();
            subMatch = baseMatch[2];
            cd.shortId += "_" + subMatch.str();
            seqan::appendValue(contigDetails, cd);
            return FastaHeaderType::RESFINDER;
        }
    }
    else if (std::regex_match(lineTr, baseMatch, regexAMR))
    {
        if (baseMatch.size() == 4 && baseMatch[1].length() != 0 && baseMatch[2].length() != 0 && baseMatch[3].length() != 0)
        {
            cd.full = lineTr.substr(1);
            cd.shortId = cd.full;
            //@TODO wait until format was fixed to unique ids
//            std::ssub_match subMatch = baseMatch[1];
//            cd.shortId = subMatch.str();
//            subMatch = baseMatch[3];
//            cd.shortId += "_" + subMatch.str();
            seqan::appendValue(contigDetails, cd);
            return FastaHeaderType::AMR;
        }
    }
    else
    {
        cd.full = lineTr.substr(1);
        cd.shortId = cd.full;
        seqan::appendValue(contigDetails, cd);
        return FastaHeaderType::UNKNOWN;
    }
}

//load the full fasta header line and the short symbol and check for valid known id formatting scheme
template<typename TContigDetails>
void extractDetailedContigInfo(TContigDetails & contigDetails, const seqan::CharString filename)
{
    std::ifstream in(seqan::toCString(filename));
    std::string line;
    while (std::getline(in, line))
    {
        if(!line.empty() && line.front() == '>')
        {
            FastaHeaderType ht = parseFastaHeader(contigDetails, line);            
            if (ht == FastaHeaderType::UNKNOWN)
            {
                std::cerr << "WARNING - unknown fasta format. Gene name cannot be inferred:\n";
                std::cerr << line << '\n';
            }
        }
    }
}

template <typename TDnaString>
bool inferGeneInfo(ContigGeneInfo& info, const TDnaString & seq)
{
    //try all six frames
    seqan::StringSet<seqan::String<seqan::AminoAcid>, seqan::Owner<seqan::ConcatDirect<> > > aaSeqs;
    seqan::translate(aaSeqs, seq, seqan::TranslationFrames::SIX_FRAME, seqan::GeneticCode<seqan::PROKARYOTE>());

    size_t frameId = 0;
    for(size_t r = 0; r < 2; ++r)
    {
        for(size_t f = 0; f < 3; ++f, ++frameId)
        {
            TDnaString startC;
            if(!r)
            {
                startC = seqan::infix(seq, f, f+3);
            }
            else
            {
                const auto tmp = seqan::infix(seq, length(seq)-3-f, length(seq)-f);
                startC = seqan::reverseComplementString(tmp);
//                std::cout << tmp << ":: " << startC << '\n';
            }
            if ((startC == "ATG" || startC == "GTG" || startC == "TTG") && seqan::back(aaSeqs[frameId]) == '*')
            {
                if(std::find(begin(aaSeqs[frameId]), end(aaSeqs[frameId]), '*') == end(aaSeqs[frameId])-1)
                {
                    info.frame = f;
                    info.reversed = r;
                    return true;
                }
            }
        }
    }
    return false;
}

//GENE ID MODE
//returns the number of clusters
template <typename TFragmentStore, typename TContigInfos>
unsigned loadContigData(TFragmentStore & fragStore,
                        TContigInfos & contigInfos,
                        TContigTypeList & contigTypes,
                        seqan::String<seqan::Pair<unsigned,unsigned>> & contigToAllele,
                        const Options & opts)
{
    typedef typename TFragmentStore::TContigNameStore TContigNameStore;

    const std::string markerHomolog = "HOM_";
    const std::string markerVariance = "VAR_";

    unsigned maxClusterId = 0;
    const bool useClusters = !empty(opts.clusterFile);
    std::cerr << "useC " << useClusters << '\n';

    //--------------------------------
    //--load cluster dictionary if provided
    //--------------------------------
    std::map<std::string, std::pair<unsigned,unsigned>> clusterMap;

    if (useClusters)
    {
        std::ifstream clusters(toCString(opts.clusterFile));
        if (clusters.is_open())
        {
            unsigned id, geneNr;
            std::string gene;
            while(clusters >> id >> geneNr >> gene)
            {
                clusterMap[gene] = std::make_pair(id, geneNr);
                maxClusterId = std::max(id, maxClusterId);
            }
            clusters.close();
        }
        else
        {
            std::cerr << "Error opening cluster file " << opts.clusterFile << '\n';
            exit(1);
        }
    }

    //--------------------------------
    //--load allele sequences in contig store
    //--------------------------------
    loadContigs(fragStore, opts.alleleFileName);

    //check for valid genes (open reading frame?, reversed?)
    seqan::resize(contigInfos, length(fragStore.contigStore));
    for (size_t i = 0; i < length(fragStore.contigStore); ++i)
    {
        bool valid = inferGeneInfo(contigInfos[i], fragStore.contigStore[i].seq);

        if(!valid)
        {
            std::cerr << "Gene could not be inferred in all 6 reading frames for " << fragStore.contigNameStore[i] << '\n';
        }
//        else
//        {
//            std::cerr << fragStore.contigNameStore[i] << " frame: " << geneInfo.frame << " reversed?: " << geneInfo.reversed << '\n';
//        }
    }



    TContigNameStore contigNames = fragStore.contigNameStore;
    resize(contigTypes, length(contigNames));
    std::cerr << "number of contigs: " << length(contigNames) << '\n';
    //Order of contigs in sam/bam should be the same as in reference fasta file

    resize(contigToAllele, length(contigNames));
    for (size_t i = 0; i < length(contigNames); ++i)
    {
        std::string contigStd(toCString(contigNames[i])); //convert to string to use find_last_of
        if (contigStd.find(markerHomolog) == 0)
        {
            contigTypes[i] = HOMOLOG;
        }
        else if (contigStd.find(markerVariance) == 0)
        {
            contigTypes[i] = VARIANT;
        }
        else
        {
            contigTypes[i] = HOMOLOG;
            //std::cerr << "Invalid contig name! Expected marker \"" << markerHomolog << "\" or \"" << markerVariance << "\" but not found! " << contigStd<< '\n';
            //exit(1); //todo exception
        }

        if(useClusters)
        {
            const auto it = clusterMap.find(contigStd);
            if (it != clusterMap.end())
            {
                contigToAllele[i].i1 = it->second.first;
                contigToAllele[i].i2 = it->second.second;
            }
            else
            {
                std::cerr << "Sequence name missing in cluster file. Aborting." << contigStd << '\n';
                throw std::runtime_error(std::string("Sequence name ") + contigStd + " missing in cluster file. Aborting.");
            }
        }
        else
        {
            contigToAllele[i].i1 = i;
            contigToAllele[i].i2 = 1;
        }
    }

    if (!useClusters)
        return length(contigNames);
    else
        return maxClusterId + 1;
}



//MLST MODE
template<typename TFragmentStore>
void loadContigData(TFragmentStore & fragStore,
                    TSequenceTypeList & types,
                    seqan::String<seqan::CharString> & locusNames,
                    seqan::String<seqan::Pair<unsigned,unsigned>> & contigToAllele,
                    const Options & opts)
{
    typedef typename TFragmentStore::TContigNameStore TContigNameStore;
    //--------------------------------
    //--load allele sequences in contig store
    //--------------------------------
    loadContigs(fragStore, opts.alleleFileName);

    TContigNameStore contigNames = fragStore.contigNameStore;
    //Order of contigs in sam/bam should be the same as in reference fasta file

    seqan::CharString lastLocusName("");
    resize(contigToAllele, length(contigNames));
    for (size_t i = 0; i < length(contigNames); ++i)
    {
        std::string contigStd(toCString(contigNames[i])); //convert to string to use find_last_of
        size_t delimPos = contigStd.find_last_of(opts.delim);
        if (delimPos == std::string::npos)
        {
            std::cerr << "Invalid contig name! Expected delimiter \"" << opts.delim << "-\" but not found! " << contigStd<< '\n';
            exit(1); //todo exception
        }
        seqan::CharString locusName(contigStd.substr(0, delimPos));
        if (locusName != lastLocusName)
        {
            appendValue(locusNames, locusName);
            lastLocusName = locusName;
        }
        unsigned alleleId = stoi(contigStd.substr(delimPos+1));
        contigToAllele[i].i1 = length(locusNames) - 1;
        contigToAllele[i].i2 = alleleId;
    }

    //--------------------------------
    //--load sequence type information
    //--------------------------------
    if (!empty(opts.stFileName))
    {
        std::fstream stream(toCString(opts.stFileName), std::ios::binary | std::ios::in);
        if (!stream.good())
        {
            std::cerr << "Unable to open sequence type file " << opts.stFileName << '\n';
            exit(1);
        }

        //read header containing the locus names
        seqan::String<seqan::CharString> columnNames;
        std::string header, cell;
        std::getline(stream, header);
        std::istringstream headerStream(header);

        while(std::getline(headerStream,cell, '\t'))
            appendValue(columnNames, cell);

        seqan::String<unsigned> indexMap; //to capture the case that order csv file is not alphabetical
        resize(indexMap, length(columnNames) - 1); //-1 as we skip the clonal-complex column
        const unsigned clonalColId = length(columnNames) - 1; //column id of clonal complex info

        for (size_t i = 1; i < length(columnNames)-1; ++i)
        {
            const auto it = std::find(begin(locusNames), end(locusNames), columnNames[i]);
            if(it != end(locusNames))
                indexMap[i] =  it - begin(locusNames);
            else
            {
                std::cerr << "unknown locus id: " << columnNames[i] << " --ABORT\n";
                exit(1);
            }
        }

        //get the number of sequence types
        std::string maxSt, line;
        while(std::getline(stream, line))
        {
            std::istringstream lineStream(line);
            lineStream >> maxSt;
        }
        size_t numSeqTypes = std::stoi(maxSt); //assume that sequence types are sorted

        resize(types.sequenceTypes, numSeqTypes + 1);
        for (auto & s : types.sequenceTypes)
            resize(s, length(locusNames), -1);
        resize(types.clonal_complex, numSeqTypes + 1);

        stream.clear(); //reset stream
        stream.seekg(0, std::ios::beg);
        std::getline(stream, line); //skip header

        while(std::getline(stream, line))
        {
            std::istringstream lineStream(line);
            std::string cell;

            size_t colId = 0;
            size_t stNum = 0;
            while(std::getline(lineStream, cell, '\t'))
            {
                //std::cout << colId << '\t' << cell << '\n';
                if (colId == 0)
                {
                    stNum = stoi(cell);
                }
                else if (colId == clonalColId)
                {
                    types.clonal_complex[stNum] = cell;
                }
                else
                {
                    types.sequenceTypes[stNum][indexMap[colId]] = stoi(cell);
                }
                ++colId;
            }
        }
    }
}

template<typename TFragmentStore>
void checkAlleles(seqan::String<seqan::Pair<unsigned,unsigned>> & contigToLocus,
                  const TFragmentStore & fragStore,
                  const seqan::String<seqan::CharString> & locusNames,
                  const Options & opts)
{
    seqan::String<seqan::Pair<size_t, size_t>> lenCnts;
    seqan::resize(lenCnts, length(locusNames), seqan::Pair<size_t, size_t>(0,0));

    const auto & contigStore = fragStore.contigStore;
    size_t numContigs = length(fragStore.contigStore);

    //get summed lengths per locus
    for (size_t i = 0; i < numContigs; ++i)
    {
        size_t locusId = contigToLocus[i].i1;
        lenCnts[locusId].i1 += length(contigStore[i].seq);
        ++lenCnts[locusId].i2;
    }

    //convert to avg. length per locus
    seqan::String<double> avgLens;
    seqan::resize(avgLens, length(lenCnts));
    for (size_t i = 0; i < length(avgLens); ++i)
    {
        avgLens[i] = static_cast<double>(lenCnts[i].i1) / lenCnts[i].i2;
    }

    //invalidate all alleles with strong deviation from avg length
    const double threshold = opts.alleleFilterCutoff / 100.;
    for (size_t i = 0; i < numContigs; ++i)
    {
        size_t locusId = contigToLocus[i].i1;
        double relDev = abs(length(contigStore[i].seq) - avgLens[locusId]) / avgLens[locusId];
        if (relDev > threshold)
        {
            contigToLocus[i].i1 = -1;
            std::cerr << "Ignoring allele " << fragStore.contigNameStore[i] << " due to significant length deviation from locus mean (" << 100 * relDev << "%)\n";
        }
    }
}

#endif // CONTIG_LOADING_H
