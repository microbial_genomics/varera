//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#ifndef SNP_FILE_H
#define SNP_FILE_H

#include <snp_record.h>

#include <seqan/basic.h>
#include <seqan/sequence.h>
#include <seqan/stream.h>
#include <seqan/translation.h>

template<typename TContigNameStore>
int readSnpFile(KnownSnps<CardSnpRecord> & snpRecords,
                const TContigNameStore & nameStore,
                const seqan::CharString & filename)
{
    //map accessions to contigIds
    std::map<CardSnpRecord::TAccessionType, size_t> accToId;
    for (size_t i = 0; i < seqan::length(nameStore); ++i)
    {
        CardSnpRecord::TAccessionType acc;
        if (geneNameToAccessionType(acc, nameStore[i], CardSnpRecord()))
        {
            accToId[acc] = i;
        }
    }

    std::fstream stream(seqan::toCString(filename), std::ios::binary | std::ios::in);
    if (!stream.good())
        return 1;

    std::string buffer;
    std::getline(stream, buffer); //skip header
    buffer.clear();
    while (std::getline(stream, buffer))
    {
        //remove " symbols
        buffer.erase(std::remove(buffer.begin(), buffer.end(), '"'), buffer.end());

        seqan::StringSet<seqan::CharString> tokens;
        seqan::strSplit(tokens, buffer.c_str(), seqan::EqualsChar<'\t'>());

        if (seqan::length(tokens) != 5)
        {
            std::cerr << "Invalid row in snp-file: \n" << buffer << '\n';
            return 1;
        }

        CardSnpRecord record;
        std::istringstream isAcc(seqan::toCString(tokens[0]));
        isAcc >> record.accession;

        //check whether this SNP accession was found in the input genes
        if(!accToId.count(record.accession))
        {
            std::cerr << "WARNING - Accession " << record.accession << " not found in input genes fasta file!\n";
            continue;
        }

        record.name = tokens[1];
        record.modelType = tokens[2];

        if (tokens[3] == "single resistance variant")
            record.paramType = CardSnpRecord::SINGLE_RESISTANCE_VARIANT;
        else if (tokens[3] == "multiple resistance variants")
            record.paramType = CardSnpRecord::MULTIPLE_RESISTANCE_VARIANTS;
        else if (tokens[3] == "frameshift mutation")
            record.paramType = CardSnpRecord::FRAMESHIFT_MUTATION;
        else if (tokens[3] == "co-dependent single resistance variant")
            record.paramType = CardSnpRecord::CO_DEPENDENT_SINGLE_RESISTANCE_VARIANT;
        else if (tokens[3] == "nonsense mutation")
            record.paramType = CardSnpRecord::NONSENSE_MUTATION;
        else
        {
            std::cerr << "Unknown Mutation mode: " << buffer[3] << '\n';
            return 1;
        }

        if(record.paramType != CardSnpRecord::SINGLE_RESISTANCE_VARIANT &&
                record.paramType != CardSnpRecord::MULTIPLE_RESISTANCE_VARIANTS &&
                record.paramType != CardSnpRecord::NONSENSE_MUTATION)
        {
            //currently we only consider these three variants
            continue;
        }

        //in case of multiple resistance variants they are comma separated
        seqan::StringSet<seqan::CharString> tokensMut;
        seqan::strSplit(tokensMut, tokens[4], seqan::EqualsChar<','>());

        for (size_t i = 0; i < seqan::length(tokensMut); ++i)
        {
            CardSnpRecord::Mutation mut;
            std::istringstream isMut(seqan::toCString(tokensMut[i]));
            isMut >> std::skipws >> mut.from >> mut.pos;
            --mut.pos; //positions in snp file are 1-based
            if (record.paramType != CardSnpRecord::NONSENSE_MUTATION)
            {
                isMut >> mut.to;
            }
            else
            {
                mut.to = '*'; //stop codon
            }
            seqan::appendValue(record.mutations, mut);
        }

        snpRecords.knownSnps.insert(std::make_pair(accToId[record.accession], std::move(record)));
    }

    return snpRecords.knownSnps.size();
}

//inline size_t getNucPosFromAAPos(size_t aaPos)
//{
//    return aaPos * 3;
//}

//template<typename TSingleAlleleStats>
//inline seqan::AminoAcid getConsensusForPos(const TSingleAlleleStats & allele, size_t pos)
//{
//    seqan::Dna5String triplet;
//    for(size_t p = pos; p < pos + 3; ++p)
//    {
//        size_t maxNucSupport = 0, totalSupport = 0;
//        size_t maxNucId = -1;
//        for(size_t nuc = 0; nuc < 4; ++nuc)
//        {
//            if (allele.profile[pos][nuc] > maxNucSupport)
//            {
//                maxNucSupport = allele.profile[pos][nuc];
//                maxNucId = nuc;
//            }
//            totalSupport += allele.profile[pos][nuc];
//        }
//        if(maxNucSupport > (totalSupport + allele.profile[pos][5]) / 2)
//        {
//            seqan::appendValue(triplet, static_cast<seqan::Dna5>(maxNucId));
//        }
//        else
//        {
//            return seqan::unknownValue<seqan::AminoAcid>();
//        }
//    }

//    seqan::String<seqan::AminoAcid> aa;
//    seqan::translate(aa, triplet);

//    return aa[0];
//}

//template<typename TContigSeq, typename TSingleAlleleStats>
//int evaluateSnp(seqan::String<bool> & snpNucPos,
//                const TSingleAlleleStats & allele,
//                const TContigSeq & contigSeq,
//                const KnownSnps<CardSnpRecord> & knownSnps)
//{
//    typedef KnownSnps<CardSnpRecord>::TSnpIter TSnpIter;

//    //auto geneAcc = geneNameToAccessionType(contigNameStore[allele.contigId], CardSnpRecord() /*tag*/);

//    TSnpIter beginIt, endIt;
//    bool isRes = false;
//    size_t numSnps = knownSnps.getSnpsForContig(beginIt, endIt, allele.contigId);
//    if(numSnps == 0)
//    {
//        //if no known snps for gene -> quit and return true
//        return true;
//    }

//    for(auto it1 = beginIt; it1 != endIt; ++it1)
//    {
//        seqan::String<size_t> localSnpNucPos;
//        bool allSnpsExist = true;
//        const auto & mutations = it1->second.mutations;
//        for (auto it2 = begin(mutations); it2 != end(mutations); ++it2)
//        {
//            size_t nucPos = (it2->pos - 1) * 3; //todo check whether in snp file counting starts at 1 or 0

//            //get the consensus of the corresponding triplet
//            seqan::AminoAcid aaCons = getConsensusForPos(allele, nucPos);
//            //for sanity check also get the reference amino acid
//            seqan::String<seqan::AminoAcid> aaRef;
//            seqan::translate(aaRef, seqan::infix(contigSeq, nucPos, nucPos+3));
//            if(aaRef[0] != it2->from)
//            {
//                std::cerr << "Warning reference base and snp \"from\" base mismatch!\n" << aaRef[0] << " vs. " << it2->from << '\n';
//                exit(1);
//            }
//            if(it2->to != aaCons)
//            {
//                allSnpsExist = false;
//                break;
//            }
//            seqan::appendValue(localSnpNucPos, nucPos);
//            seqan::appendValue(localSnpNucPos, nucPos + 1);
//            seqan::appendValue(localSnpNucPos, nucPos + 2);
//        }
//        if (allSnpsExist) // we found one/multiple res. conferring mutations
//        {
//            for (const auto pos : localSnpNucPos)
//            {
//                snpNucPos[pos] = true;
//            }
//            isRes = true;
//        }
//    }
//    return isRes;
//}


#endif // SNP_FILE_H
