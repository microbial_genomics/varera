//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#ifndef CONTIG_H
#define CONTIG_H

#include <seqan/store.h>

//type according to card database
enum ContigType
{
    HOMOLOG,
    VARIANT
};

typedef seqan::String<ContigType> TContigTypeList;

template< typename TSpec = void>
struct MyFragStoreContigConfig : public seqan::FragmentStoreConfig<TSpec>
{
    typedef seqan::String<seqan::Dna5>    TReadSeq;
    typedef seqan::String<seqan::Iupac>    TContigSeq;
};

typedef seqan::FragmentStore<void, MyFragStoreContigConfig<void>> TFragmentStoreContigs;

//basic information required for amino acid related statistics and SNP analysis
struct ContigGeneInfo
{
    unsigned frame = -1; // -1 defines an invalid gene where no orf was detected
    bool reversed = false;
};

//
struct ContigHeaderDetail{
    std::string full;
    std::string shortId;
};

//fasta header formatting types. Currently supporting Card and resfinder/virulencefinder formatting
enum FastaHeaderType{
    CARD,
    RESFINDER,
    AMR,
    UNKNOWN
};

#endif // CONTIG_H
