//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#ifndef KMERCOUNTING_H
#define KMERCOUNTING_H

#include <options.h>
#include <helper.h>

#include <unordered_set>

//seqan includes
#include <seqan/store.h>
#include <seqan/sequence.h>

struct SingleAlleleQStats
{
    enum TIssues
    {
        LOW_DEPTH,
        NUM_ISSUES,
    };

    typedef std::bitset<NUM_ISSUES +1> TIssueMask;

    unsigned locusId = -1;
    unsigned alleleId = -1;
    unsigned contigId = -1;
    unsigned numHits = 0;
    double numHitsPerBase = 0.;
    double avgDepth = 0.;

    TIssueMask issues;
};

template<typename TAlleleCounts, typename TSeqType, typename TIndex>
void updateQgramMatches(TAlleleCounts & alleleCounts, TSeqType & seq, TIndex & index, unsigned q, double minSupport)
{
    TAlleleCounts tmpCountRead; //check the number of hits of the read per allele
    resize(tmpCountRead, length(alleleCounts), 0);
    std::unordered_set<unsigned> tmpHitRead(length(alleleCounts)); // record which alleles have been hit by the read at least once

    //get the center qgram
    const size_t adjustLen = length(seq) - q + 1;
    size_t center = adjustLen / 2;

    hash(indexShape(index), begin(seq) + center);
    //only count matches if the center q-gram matches at least once
    if(countOccurrences(index, indexShape(index)))
    {
        hashInit(indexShape(index), begin(seq));
        for(size_t i = 0; i < adjustLen; ++i)
        {
            hashNext(indexShape(index), begin(seq) + i);
            const auto & occs = countOccurrencesMultiple(index, indexShape(index));
            for (const auto & occ : occs)
            {
                //just increment - no double counting
                ++tmpCountRead[occ.i1];
                tmpHitRead.insert(occ.i1);
            }
        }

        const unsigned minReqHits = minSupport * adjustLen; //required number of q-hits per read to add q-gram counts
        for (auto allele : tmpHitRead)
        {
            if (tmpCountRead[allele] >= minReqHits)
                alleleCounts[allele] += tmpCountRead[allele];
        }
    }
}



template <typename TAlleleCounts, typename TIndex>
void kmerCounting(TAlleleCounts & alleleCounts,
                  seqan::SeqFileIn & fastaFile,
                  TIndex & qgramIndex,
                  const Options &opts)
{
    const size_t batchSize = 1e6;
    //build q-gram index of contigs

    //resize(alleleCounts, length(contigStore),0);

    //iterate over reads to find matching k-mers
    seqan::StringSet<seqan::CharString> ids;
    seqan::StringSet<seqan::Dna5String> seqs;
    seqan::StringSet<seqan::CharString> quals;
    reserve(ids, batchSize);
    reserve(seqs, batchSize);
    reserve(quals, batchSize);
    while(!atEnd(fastaFile))
    {
        readRecords(ids, seqs, quals, fastaFile, batchSize);

        //now iterate over single reads and perform k-mer matching
        for(size_t i = 0; i < length(seqs); ++i)
        {
            updateQgramMatches(alleleCounts, seqs[i], qgramIndex, opts.qLen, opts.qMinSupport);
            reverseComplement(seqs[i]);
            updateQgramMatches(alleleCounts, seqs[i], qgramIndex, opts.qLen, opts.qMinSupport);
        }
        clear(seqs);
        clear(ids);
        clear(quals);
    }
}

#endif // KMERCOUNTING_H
