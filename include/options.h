// ==========================================================================
//                 MLSTTool - Tool for MLST based on NGS data
// ==========================================================================
// Copyright (c) 2017, Sandro Andreotti, MODAL AG
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of Knut Reinert or the FU Berlin nor the names of
//       its contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL KNUT REINERT OR THE FU BERLIN BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
// DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================
#ifndef OPTIONS_H
#define OPTIONS_H

#include <seqan/sequence.h>

struct Options
{
    enum Mode{
        MLST_Q_GRAM,
        MLST_MAPPING,
        MLST_HYBRID,
        RESISTANCE_GENES_HOM,
        RESISTANCE_GENES_VAR
    };

    Mode mode = MLST_MAPPING;
    //general options
    seqan::CharString alleleFileName;
    seqan::CharString stFileName;
    seqan::CharString summaryOutFileName;
    char delim = '-';

    //------------------------
    //--mapping mode options--
    //------------------------

    //files
    seqan::CharString bamFileName;
    seqan::CharString covOutFileName;
    seqan::CharString snpFile;

    //params
    double alleleFilterCutoff = 10.;
    unsigned maxUnmatchedOverlaps = 10;
    unsigned terminalLength = 2;
    unsigned minTerminalDepth = 2;
    unsigned minAvgDepth = 5;
    unsigned minAvgDepthW = 10;
    double minCoverage = 0.9;
    double minCoverageW = 0.95;
    double maxDivergence = 0.02;
    double maxDivergenceW = 0.01;
    char minPhredScore = 20;
    unsigned minMappingScore = 1;

    //---------------------------
    //--q-gram counting options--
    //---------------------------
    seqan::CharString readsFileName; //for Q_GRAM mode
    seqan::CharString readsFileName2; //for Q_GRAM mode with paired-end
    unsigned qLen = 35;
    double qMinSupport = 0.5; //fraction of q-grams of a read that needs to be mapped
    double qMinAvgDepth = 2;

    //gene mode
    seqan::CharString clusterFile;
};

#endif // OPTIONS_H
