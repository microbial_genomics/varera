//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#ifndef REPORTING_H
#define REPORTING_H

#include <mapcounting.h>
#include <kmercounting.h>
#include <hybridcounting.h>
#include <contig_loading.h>

//for mapping mode
template<typename TContigStore, typename TContigNameStore>
void writeCoverageOutput(const seqan::String<SingleAlleleStats> & bestAlleles,
                         const TContigStore & contigStore,
                         const TContigNameStore & contigNameStore,
                         const Options & opts)
{
    if (!empty(opts.covOutFileName))
    {
        std::ofstream ofCov(toCString(opts.covOutFileName));
        if(ofCov.is_open())
        {
            const seqan::CharString sep = ';';
            //per position per base count for construction of nice coverage plots
            for(size_t i = 0; i < length(bestAlleles); ++i)
            {
                //const unsigned locusId = reportedContigsH[i];
                const SingleAlleleStats & currentAllele = bestAlleles[i];

                if (currentAllele.alleleId == static_cast<decltype(currentAllele.alleleId)>(-1))
                    continue;

                ofCov << '\"' << contigNameStore[currentAllele.contigId] << '\"';
                for(const auto & refBase : contigStore[currentAllele.contigId].seq)
                {
                    ofCov << sep << static_cast<int>(static_cast<seqan::Dna5>(refBase));
                }
                ofCov << '\n';
                for (size_t b = 0; b < length(currentAllele.profile); ++b)
                {
                    ofCov << '\"' << contigNameStore[currentAllele.contigId] << '\"';
                    for (size_t j = 0; j < length(currentAllele.profile[b]); ++j)
                    {
                        ofCov << sep << std::to_string(currentAllele.profile[b][j]);
                    }
                    ofCov << '\n';
                }
            }
            ofCov.close();
        }
        else
        {
            std::cerr << "error opening outfile for writing: " << opts.covOutFileName << '\n';
        }
    }
}

//for hybrid k-mer mode
template<typename TContigNameStore>
void writeCoverageOutput(const seqan::String<SingleAlleleHStats> & bestAlleles,
                         const TContigNameStore & contigNameStore,
                         const Options & opts)
{
    if (!empty(opts.covOutFileName))
    {
        std::ofstream ofCov(toCString(opts.covOutFileName));
        if(ofCov.is_open())
        {
            const seqan::CharString sep = ';';
            //per position per base count for construction of nice coverage plots
            for(size_t i = 0; i < length(bestAlleles); ++i)
            {
                const SingleAlleleHStats & currentAllele = bestAlleles[i];

                if (currentAllele.alleleId == static_cast<decltype(currentAllele.alleleId)>(-1))
                    continue;

                ofCov << '\"' << contigNameStore[currentAllele.contigId] << '\"';
                for (size_t b = 0; b < length(currentAllele.profile); ++b)
                {
                    std::ostringstream num;
                    num << std::fixed << std::setprecision(1) << currentAllele.profile[b];
                    ofCov << sep << num.str();
                }
                ofCov << '\n';
            }
            ofCov.close();
        }
        else
        {
            std::cerr << "error opening outfile for writing: " << opts.covOutFileName << '\n';
        }
    }
}

//summary table for kmer based reporting (normal and hybrid)
template<typename TSingleAlleleQHStats>
void inline fillSummaryTable(seqan::String<seqan::CharString> & line,
                             const TSingleAlleleQHStats & currentAllele,
                             const Options::Mode)
{
    //Issues
    seqan::StringSet<seqan::CharString> issuesTmp;
    if (currentAllele.issues.test(SingleAlleleQStats::LOW_DEPTH))
        appendValue(issuesTmp, "low depth");

    if (!empty(issuesTmp))
        appendValue(line, concat(issuesTmp,", "));
    else
        appendValue(line,"-");

    //stats
    appendValue(line, to_string_(currentAllele.numHits,1));
    appendValue(line, to_string_(currentAllele.avgDepth,1));
}


//summary table for mapping based reporting (specialization of templated version called for mapping)
void inline fillSummaryTable(seqan::String<seqan::CharString> & line,
                             const SingleAlleleStats & currentAllele,
                             const Options::Mode mode)
{
    //Issues
    seqan::StringSet<seqan::CharString> issuesTmp;
    if (currentAllele.issues.test(SingleAlleleStats::HIGH_DIVERGENCE))
        appendValue(issuesTmp, "high div.");
    if (currentAllele.issues.test(SingleAlleleStats::LOW_COVERAGE))
        appendValue(issuesTmp, "low cov.");
    if (currentAllele.issues.test(SingleAlleleStats::LOW_DEPTH))
        appendValue(issuesTmp, "low depth");
    if (currentAllele.issues.test(SingleAlleleStats::LOW_DEPTH_3PRIME))
        appendValue(issuesTmp, R"(low depth 3\\`)");
    if (currentAllele.issues.test(SingleAlleleStats::LOW_DEPTH_5PRIME))
        appendValue(issuesTmp, R"(low depth 5\\`)");
    if (currentAllele.issues.test(SingleAlleleStats::INVALID_GENE))
        appendValue(issuesTmp, "no ORF found");

    if (!empty(issuesTmp))
        appendValue(line, seqan::concat(issuesTmp,", "));
    else
        appendValue(line,"-");

    std::ostringstream mismatchss;
    if (mode >= Options::RESISTANCE_GENES_HOM && !currentAllele.issues.test(SingleAlleleStats::INVALID_GENE))
    {
        //get number of silent/non silent snps and number of AA replacements
        size_t mutAA = 0, knownMutAA = 0, snpDNASil = 0, snpDNANonSil = 0;
        for(const auto & m : currentAllele.mutations)
        {
            if (m.fromAA != m.toAA)
            {
                ++mutAA;
                snpDNANonSil += length(m.mutationsDNA);
                if (m.isKnown)
                    ++knownMutAA;
            }
            else
            {
                snpDNASil += length(m.mutationsDNA);
            }
        }
        mismatchss << snpDNANonSil + snpDNASil << '/' << snpDNANonSil << '/' << mutAA << '/' << knownMutAA;
    }
    else //for MLST (and invalid resistance genes in) simply take the value stored for the allele
    {
        mismatchss << currentAllele.numMismatches;
    }

    //stats
    appendValue(line, to_string_(100 * currentAllele.coverage, 1));
    appendValue(line, to_string_(currentAllele.avgDepth, 1));
    appendValue(line, mismatchss.str());
    appendValue(line, to_string_(currentAllele.numInsertions, 0));
    appendValue(line, to_string_(currentAllele.numDeletions, 0));
    appendValue(line, to_string_(100 * currentAllele.divergence,2));

    //mutations
    std::ostringstream mutss;
    bool first = true;
    for(size_t i = 0; i < length(currentAllele.mutations); ++i)
    {
        const MutationAA & m = currentAllele.mutations[i];
        if (mode >= Options::RESISTANCE_GENES_HOM) //for resistance genes list AA mismatches and more detailed counts
        {
            if (m.fromAA != m.toAA)
            {
                if (!first)
                    mutss << ", ";
                if (!m.isKnown)
                    mutss << '!';
                mutss << m.fromAA << m.posAA << m.toAA;
                first = false;
            }
        }
        else //for MLST just list the DNA mismatches
        {
            for(const auto & mutDNA : m.mutationsDNA)
            {
                if (!first)
                    mutss << ", ";
                mutss << mutDNA.fromDNA << mutDNA.posDNA << mutDNA.toDNA;
                first = false;
            }
        }
    }
    appendValue(line, mutss.str());
}

void inline getTableHeaderMLST(seqan::String<seqan::CharString> & header, const SingleAlleleStats & /*tag*/)
{
    const seqan::CharString col_names [] = {"Locus", "Allele", "Issues", "% Coverage", "Avg. Depth", "\\#Mismatches", "\\#Insertions", "\\#Deletions", "% Divergence", "Mutation details", "ST", "Clonal Complex"};
    for (const auto n : col_names)
        appendValue(header, n);
}

void inline getTableHeaderMLST(seqan::String<seqan::CharString> & header, const SingleAlleleQStats & /*tag*/)
{
    const seqan::CharString col_names [] = {"Locus", "Allele", "Issues", "\\#Hits", "Avg. Depth", "ST", "Clonal Complex"};
    for (const auto n : col_names)
        appendValue(header, n);
}

void inline getTableHeaderMLST(seqan::String<seqan::CharString> & header, const SingleAlleleHStats & /*tag*/)
{
    const seqan::CharString col_names [] = {"Locus", "Allele", "Issues", "\\#Hits", "Avg. Depth", "ST", "Clonal Complex"};
    for (const auto n : col_names)
        appendValue(header, n);
}

template<typename TContigNameStore, typename TSingleAlleleStats>
void writeSummaryOutput(const seqan::String<TSingleAlleleStats> & bestAlleles,
                 const TContigNameStore & contigNameStore,
                 const seqan::String<seqan::CharString> & locusNameStore,
                 const TSequenceTypeList & seqTypes,
                 const Options & opts)
{

    using namespace seqan;
    //------------------------------------
    //-----------SUMMARY OUTPUT-----------
    //------------------------------------
    String<String<CharString>> rowsH;
    String<CharString> header;
    if (opts.mode >= Options::RESISTANCE_GENES_HOM)
    {
        const CharString col_names [] = {"Gene","Short name", "Group", "Issues", "% Coverage", "Avg. Depth", "\\#Mismatches", "\\#Insertions", "\\#Deletions", "% Divergence", "Mutation details"};
        for (const auto n : col_names)
            appendValue(header, n);
    }
    else
    {
        getTableHeaderMLST(header, TSingleAlleleStats());
    }

    const unsigned numCols = length(header);
    bool missingMLSTAllele = false;

    for(size_t i = 0; i < length(bestAlleles); ++i)
    {
        const TSingleAlleleStats & currentAllele = bestAlleles[i];

        if(currentAllele.alleleId == static_cast<decltype(currentAllele.alleleId)>(-1) && opts.mode >= Options::RESISTANCE_GENES_HOM)
        {
            continue;
        }

        String<CharString> line;
        if(opts.mode >= Options::RESISTANCE_GENES_HOM)
        {
            appendValue(line, contigNameStore[currentAllele.contigId].full);
            appendValue(line, contigNameStore[currentAllele.contigId].shortId);
            appendValue(line, std::to_string(currentAllele.locusId));
        }
        else
        {
            appendValue(line, locusNameStore[i]);
            //Allele id
            if (currentAllele.alleleId != static_cast<decltype(currentAllele.alleleId)>(-1))
            {
                appendValue(line, std::to_string(currentAllele.alleleId));
            }
            else
            {
                resize(line, numCols, '-');
                appendValue(rowsH, line);
                missingMLSTAllele = true;
                continue;
            }
        }

        fillSummaryTable(line, currentAllele, opts.mode); //overloads according to allele type (mapping/kmer)
        appendValue(rowsH, line);
    }

    //Known sequence type?
    unsigned st = -1;
    if (opts.mode < Options::RESISTANCE_GENES_HOM && !missingMLSTAllele)
    {
        String<unsigned> bestAlleleIds; //for searching the sequence type
        for(const auto a : bestAlleles)
            appendValue(bestAlleleIds, a.alleleId);

        auto it = std::find(begin(seqTypes.sequenceTypes), end(seqTypes.sequenceTypes), bestAlleleIds);
        if (it != end(seqTypes.sequenceTypes))
            st = it - begin(seqTypes.sequenceTypes);

        if(st != static_cast<unsigned>(-1))
        {
            appendValue(rowsH[0], std::to_string(st));
            if (!empty(seqTypes.clonal_complex[st]))
                appendValue(rowsH[0], seqTypes.clonal_complex[st]);
            else
                appendValue(rowsH[0], "unknown");
        }
        else
        {
            appendValue(rowsH[0], "unknown"); //ST
            appendValue(rowsH[0], "unknown"); //clonal complex
        }
    }

    //write to outfile
    CharString sep = ';';
    std::ostringstream ss;
    ss << concat(header, sep) << '\n';
    for (const auto & line : rowsH)
        ss << concat(line, sep) << '\n';

    bool writeSuccess = false;
    if (!empty(opts.summaryOutFileName))
    {
        std::ofstream of(toCString(opts.summaryOutFileName));
        if(of.is_open())
        {
            of << ss.str();
            of.close();
            writeSuccess = true;
        }
        else
        {
            std::cerr << "error opening outfile for writing: " << opts.summaryOutFileName << '\n';
            std::cerr << "writing results to stdout!\n";
        }
    }
    if (!writeSuccess) //if no outfile given or opening failed write to command line
        std::cout << ss.str();
}


template<typename TContigNameStore>
void writeSummaryOutputResMatcher(const seqan::String<SingleAlleleStats> & bestAlleles,
                 const TContigNameStore & contigNameStore,
                 const Options & opts)
{
    writeSummaryOutput(bestAlleles,
                       contigNameStore,
                       seqan::String<seqan::CharString>()/*dummy for locusNameStore*/,
                       TSequenceTypeList()/*dummy for sequenceTypeList*/,
                       opts);
}

template<typename TSingleAlleleStats>
void writeSummaryOutputMLST(const seqan::String<TSingleAlleleStats> & bestAlleles,
                 const seqan::String<seqan::CharString> & locusNameStore,
                 const TSequenceTypeList & sequenceTypes,
                 const Options & opts)
{
    writeSummaryOutput(bestAlleles,
                       seqan::String<ContigHeaderDetail>() /*dummy for contigNameStore*/,
                       locusNameStore,
                       sequenceTypes,
                       opts);
}



#endif // REPORTING_H
