//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#ifndef MAPCOUNTING_H
#define MAPCOUNTING_H

#include <options.h>
#include <helper.h>
#include <snp_file.h>
#include <contig.h>
#include <dna_protein_conversions.h>

//seqan includes
#include <seqan/sequence.h>
#include <seqan/bam_io.h>
#include <seqan/translation.h>

//boost includes
#include <boost/random/binomial_distribution.hpp>
#include <boost/math/distributions/binomial.hpp>

#include <assert.h>

struct MutationDNA
{
    enum MutationType
    {
        INSERTION,
        DELETION,
        MISMATCH
    };

    MutationType mType;
    seqan::Dna5 fromDNA;
    seqan::Dna5 toDNA;
    size_t posDNA;
};

struct MutationAA
{
    seqan::AminoAcid fromAA = '*';
    seqan::AminoAcid toAA = '*';
    size_t posAA = -1;
    seqan::String<MutationDNA> mutationsDNA;
    bool isKnown = false;
};


struct SingleAlleleStats
{
    enum TIssues
    {
        LOW_DEPTH,
        LOW_DEPTH_3PRIME,
        LOW_DEPTH_5PRIME,
        HIGH_DIVERGENCE,
        LOW_COVERAGE,
        MISSING_SNPS,
        INVALID_GENE,
        NUM_ISSUES,
    };

    typedef std::bitset<NUM_ISSUES +1> TIssueMask;
    typedef seqan::StringSet<seqan::String<unsigned>> TProfile;

    unsigned contigId = -1;
    unsigned locusId = -1;
    unsigned alleleId = -1;

    double score = std::numeric_limits<double>::max();
    double avgDepth = 0.;   //average depth across complete allele
    double avgDepth5Prime = 0.; //average depth at alleles 5' end
    double avgDepth3Prime = 0.; //average depth at alleles 3' end
    double divergence = 0.; //fraction of non-matching positions
    double coverage = 0.; //fraction of allele covered

    unsigned numInsertions = 0;
    unsigned numDeletions = 0;
    unsigned numMismatches = 0;

    TIssueMask issues;
    TProfile profile;
    seqan::String<MutationAA> mutations;

    bool operator<(const SingleAlleleStats & rhs) const
    {
        return score < rhs.score;
    }
};

//test whether provided fasta and alignment file match (i.e. ref ids in bam and fasta match)
template <typename TFastaIdList>
bool validateMatchingFiles(const TFastaIdList & fastaIds, const seqan::CharString & bamFileName)
{
    //get the bam headers
    seqan::BamFileIn bamFileIn;
    if (!open(bamFileIn, toCString(bamFileName)))
    {
        throw std::runtime_error(std::string("ERROR: Could not open ") + toCString(bamFileName));
    }

    seqan::BamHeader header;
    readHeader(header, bamFileIn);
    typedef seqan::FormattedFileContext<seqan::BamFileIn, void>::Type TBamContext;
    TBamContext const & bamContext = context(bamFileIn);

    if (length(contigNames(bamContext)) != length(fastaIds))
    {
        std::cerr << length(contigNames(bamContext)) << '\t' << length(fastaIds) << '\n';
        return false;
    }

    for (size_t i = 0; i < length(contigNames(bamContext)); ++i)
    {
        if (contigNames(bamContext)[i] != fastaIds[i])
        {
            std::cerr << i << '\t' << contigNames(bamContext)[i] << '\t' << fastaIds[i] << '\n';
            return false;
        }
    }
    return true;
}

//calculate the regression between the first n elements of x and y
inline seqan::Pair<double, double> computeRegressionN(const seqan::String<double> & x, const seqan::String<double> & y, size_t n, bool intercept = true)
{
        const double sx  = std::accumulate(begin(x), begin(x)+n, 0.0);
        const double sy  = std::accumulate(begin(y), begin(y)+n, 0.0);

        //sum_i(x_i * x_i)
        const double xx = std::inner_product(begin(x), begin(x)+n, begin(x), 0.0);
        //sum_i(x_i * y_i)
        const double xy = std::inner_product(begin(x), begin(x)+n, begin(y), 0.0);

        double slope, inter;
        if (intercept)
        {
            slope = ((n * xy) - (sx * sy)) / ((n * xx) - (sx * sx));
            inter = (sy - slope * sx) / n;
        }
        else
        {
            slope = xy / xx;
            inter = 0;
        }

        return seqan::Pair<double, double>(slope, inter);
        //return std::make_pair(slope, intercept);
}



//reverve the space required to store the mapping profiles for each allele
//assign locusIds and alleleIds
template <typename TContigSeq, typename TLocusAlleleId>
void initAlleleStat(const TContigSeq & contigSeq,
                    const TLocusAlleleId & locusAlleleId,
                    SingleAlleleStats & allele)
{

    //get a fresh allele stat
    allele = SingleAlleleStats();

    size_t contigLen = length(contigSeq);
    resize(allele.profile, 7); //counts for 'A','C','G','T','N','-' and insertion
    for (int i = 0; i < 7; ++i)
    {
        resize(allele.profile[i], contigLen, 0);
        arrayFill(begin(allele.profile[i]), end(allele.profile[i]), 0);
    }
    allele.locusId = locusAlleleId.i1;
    allele.alleleId = locusAlleleId.i2;
}

//replace best allele by current candidate if it has better score or same score with less issues
void updateTopLocusAlleles(seqan::String<SingleAlleleStats> & bestAlleles, SingleAlleleStats & allele, const Options & opts)
{
    //check if current candidate is better than best so far
    size_t locusId = allele.locusId;
//    std::cout << allele.locusId << '\t' << allele.alleleId << '\t' << allele.score << '\t' << allele.coverage << '\t' << allele.avgDepth << '\t' << allele.divergence << '\n';

    if (allele.coverage < opts.minCoverage ||
            allele.divergence > opts.maxDivergence ||
            allele.avgDepth < opts.minAvgDepth ||
            allele.score > bestAlleles[locusId].score ) //no need to consider this allele
        return;

    //warnings
    if (allele.coverage < opts.minCoverageW)
        allele.issues.set(SingleAlleleStats::LOW_COVERAGE);
    if (allele.divergence > opts.maxDivergenceW)
    {
        allele.issues.set(SingleAlleleStats::HIGH_DIVERGENCE);
        //std::cout << "setting div: " << allele.divergence << '\t' << opts.maxDivergenceW << '\n';
    }
    //depth warnings
    if (allele.avgDepth < opts.minAvgDepthW)
        allele.issues.set(SingleAlleleStats::LOW_DEPTH);
    else if (allele.avgDepth3Prime < opts.minTerminalDepth)
        allele.issues.set(SingleAlleleStats::LOW_DEPTH_3PRIME);
    else if (allele.avgDepth5Prime < opts.minTerminalDepth)
        allele.issues.set(SingleAlleleStats::LOW_DEPTH_5PRIME);

    if(opts.mode == Options::RESISTANCE_GENES_VAR)
    {
        if(allele.issues.test(SingleAlleleStats::MISSING_SNPS))
            return;
    }

    if (allele.score < bestAlleles[locusId].score || allele.issues.count() < bestAlleles[locusId].issues.count())
    {
        bestAlleles[locusId] = allele;
//        std::cout << "new best\n";
    }
}


//compute the portion of unmapped read sequence overlapping the allele (not gene's 3'- or 5'-end overhang)
//return value is (left unmatched overlap, right enf unmatched overlap)
//typedef decltype(BamAlignmentRecord::cigar) TCigarString;
//typedef decltype(BamAlignmentRecordCore::beginPos) TBamAlignPos;
template <typename TBamAlignPos, typename TCigarString>
std::pair<TBamAlignPos,TBamAlignPos> getUnmatchedOverlaps(const TCigarString &cigarString, TBamAlignPos beginPos, TBamAlignPos endPos, size_t contigLength)
{
    TBamAlignPos leftUnmatched = 0;
    for (size_t i = 0; i < length(cigarString); ++i)
    {
        const char operation = cigarString[i].operation;
        if (operation == 'S' || operation == 'H')
            leftUnmatched += cigarString[i].count;
        else
            break;
    }
    TBamAlignPos rightUnmatched = 0;
    for (int i = length(cigarString) - 1; i >=0; --i)
    {
        const char operation = cigarString[i].operation;
        if (operation == 'S' || operation == 'H')
            rightUnmatched += cigarString[i].count;
        else
            break;
    }
    return std::make_pair(std::min(beginPos, leftUnmatched), std::min(static_cast<TBamAlignPos>(contigLength) - endPos, rightUnmatched));
}


void penalizeGaps(seqan::String<unsigned> & nonMatches, const seqan::String<unsigned> & matches, bool perBasePenalty = false)
{
//    unsigned numPens = 0;
    //check for truncation or internal deletions (i.e. uncovered regions)
    size_t contigLen = length(matches);
    size_t lastCoveredPos = -1;
    for (size_t pos = 0; pos < contigLen; ++pos)
    {
        //compute depth
        //skip positions that are uncovered. For each uncovered interval [i,j] we will compute the depth as:
        //  1. avg of positions i-1, j+1 (store at positon i) -- internal region
        //  2. avg of positions j+1, j+2 (store at position j) -- 5' truncation
        //  3. avg of positions i-2, i-1 (store at position i) -- 3' truncation
        const unsigned depthPos = nonMatches[pos] + matches[pos];
        if (depthPos == 0) //skip uncovered positions
        {
            continue;
        }
        else
        {
            //std::cout << "first covered pos: " << pos << '\n';
            //check for end of uncovered region
            if(lastCoveredPos != pos-1) //we just reached the end of an uncovered region.
            {
                if (lastCoveredPos == static_cast<decltype(lastCoveredPos)>(-1)) // 5' truncation (CASE 2)
                {
                    unsigned nonMatchesPos = -1;
                    if(pos < contigLen - 1)
                        nonMatchesPos = (depthPos + matches[pos+1] + nonMatches[pos+1]) / 2.;
                    else
                        nonMatchesPos = nonMatches[pos];

                    if(perBasePenalty)
                    {
                        for (size_t i = 0; i < pos; ++i)
                            nonMatches[i] = nonMatchesPos;
//                        numPens += pos;
//                        std::cout << 0 << " - " << pos << '\n';
                    }
                    else
                    {
                        nonMatches[pos-1] = nonMatchesPos;
//                        numPens += 1;
                    }

                    lastCoveredPos = pos;
#ifdef DEBUG
                    std::cerr << pos << " " << "5' truncation " << nonMatches[pos-1] << "\n";
#endif
                }
                else //internal deletion (CASE 1)
                {
#ifdef SRST2
                    //take avg of two bases in the right (why?)
                    unsigned nonMatches = (matches[pos+1] + nonMatches[pos+1] + depthPos) / 2.;
#else
                    //take average to left and right base
                    unsigned nonMatchesPos = 0.5 + (matches[lastCoveredPos] + nonMatches[lastCoveredPos] + depthPos) / 2.;
                    if(perBasePenalty)
                    {
                        for (size_t i = lastCoveredPos + 1; i < pos; ++i)
                            nonMatches[i] = nonMatchesPos;
//                        numPens += pos - lastCoveredPos - 1;
//                        std::cout << lastCoveredPos + 1 << " - " << pos << '\n';
                    }
                    else
                    {
                        nonMatches[pos-1] = nonMatchesPos;
//                        numPens += 1;
                    }
#endif
                    lastCoveredPos = pos;
#ifdef DEBUG
                    std::cerr << pos << " " <<  "internal deletion " << nonMatches[pos-1] << '\n';
#endif
                }
            }
            lastCoveredPos = pos;
#ifdef DEBUG
            if (insertionsRef[pos])
                std::cerr << pos+1 << " insertion " << " " << insertionsRef[pos] << '\n';
            std::cerr << pos+1 << " match " << " " << matches[pos] << " " << nonMatches[pos] << '\n';
#endif
        }
    }
    //check for 3' truncation (CASE 3)
    if (lastCoveredPos != contigLen - 1) // 3' end truncation
    {
        if (lastCoveredPos != static_cast<decltype(lastCoveredPos)>(-1))
        {
            unsigned nonMatchesPos = -1;
            if(lastCoveredPos > 0)
                nonMatchesPos = (matches[lastCoveredPos] + nonMatches[lastCoveredPos] + matches[lastCoveredPos - 1] + nonMatches[lastCoveredPos - 1] )/ 2.;
            else
                nonMatchesPos = matches[lastCoveredPos] + nonMatches[lastCoveredPos];

            if(perBasePenalty)
            {
                for (size_t i = lastCoveredPos + 1; i < contigLen; ++i)
                    nonMatches[i] = nonMatchesPos;
//                numPens += contigLen - lastCoveredPos - 1;
//                std::cout << lastCoveredPos + 1 << " - " << contigLen << '\n';
            }
            else
            {
                nonMatches[contigLen - 1] = nonMatchesPos;
//                numPens += 1;
            }
#ifdef DEBUG
            std::cerr << contigLen << " " << "3' truncation " << nonMatches[contigLen - 1] << '\n';
#endif
        }
        else
        {
#ifdef DEBUG
            std::cerr << "no aligned bases -- skipping allele \n";
#endif
            return;
        }
    }
}

template<typename TContigSeq, typename TContigInfo, typename TSingleAlleleStats>
int getObserverdMutations(TSingleAlleleStats & allele,
                      const seqan::String<MutationDNA> mutationsDNA,
                      const TContigSeq & contigSeq,
                      const TContigInfo & contigInfo,
                      const KnownSnps<CardSnpRecord> & knownSnps)
{
    typedef KnownSnps<CardSnpRecord>::TSnpIter TSnpIter;
    TSnpIter tmpIt, tmp2It;
    size_t numKnownSnps = knownSnps.getSnpsForContig(tmpIt, tmp2It, allele.contigId);

    if(numKnownSnps && contigInfo.frame == static_cast<decltype(contigInfo.frame)>(-1))
    {
        return 0; //this gene was detected as invalid -> we cannot deduce mutations!
    }

    clear(allele.mutations);

    size_t winLen = 0;
    for(size_t i = 0; i < seqan::length(mutationsDNA); ++i)
    {
        if (mutationsDNA[i].mType == MutationDNA::MISMATCH)
        {
            size_t codonPos = getCodonPos(length(contigSeq), mutationsDNA[i].posDNA, contigInfo.frame, contigInfo.reversed);

            if(codonPos == static_cast<decltype(codonPos)>(-1))
            {
                std::cerr << "Warning: Skipping invalid aaPos. Probably due to invalid gene!\n";
                continue;
            }
            size_t nextCodonPos = -1;
            if (i < seqan::length(mutationsDNA) - 1)
            {
                nextCodonPos = getCodonPos(length(contigSeq), mutationsDNA[i+1].posDNA, contigInfo.frame, contigInfo.reversed);
            }

            if (codonPos == nextCodonPos)
            {
                ++winLen;
            }
            else
            {
                //size_t codonPos = getCodonPos(length(contigSeq), mutationsDNA[i].posDNA, contigInfo.frame, contigInfo.reversed);
                if(codonPos == static_cast<decltype(codonPos)>(-1))
                {
                    std::cerr << "Warning: Skipping invalid codonPos. Probably due to invalid gene!\n";
                    std::cerr << length(contigSeq) << '\t' <<  mutationsDNA[i].posDNA << '\t'
                              << contigInfo.frame << '\t' << contigInfo.reversed << '\n';
                    continue;
                }

                seqan::Dna5String triplet(seqan::infix(contigSeq, codonPos, codonPos + 3));
                for(size_t w = 0; w <= winLen; ++w)
                {
                    triplet[mutationsDNA[i - w].posDNA - codonPos] = mutationsDNA[i - w].toDNA;
                }

                seqan::String<seqan::AminoAcid> aaMut, aaOrig;
                if(contigInfo.reversed)
                {
                    seqan::translate(aaOrig, seqan::reverseComplementString(seqan::infix(contigSeq, codonPos, codonPos + 3)));
                    seqan::reverseComplement(triplet);
                    seqan::translate(aaMut, triplet);
                }
                else
                {
                    seqan::translate(aaOrig, seqan::infix(contigSeq, codonPos, codonPos + 3));
                    seqan::translate(aaMut, triplet);
                }

                MutationAA mutAA;
                mutAA.fromAA = aaOrig[0];
                mutAA.toAA = aaMut[0];
                mutAA.posAA = getAAPos(length(contigSeq), mutationsDNA[i].posDNA, contigInfo.frame, contigInfo.reversed);
                //append the associated DNA mutations
                seqan::append(mutAA.mutationsDNA, seqan::infix(mutationsDNA, i-winLen,i+1));
                seqan::appendValue(allele.mutations, mutAA);

                //reset window length
                winLen = 0;
            }
        }
        else
        {
            winLen = 0; //an indel destroys the mutation anyways
            //skip until next mismatch for a different amino acid
            size_t codonPos = getCodonPos(length(contigSeq), mutationsDNA[i].posDNA, contigInfo.frame, contigInfo.reversed);
            for (size_t skip = 1; i + skip < length(mutationsDNA); ++skip)
            {
                if (mutationsDNA[i + skip].mType == MutationDNA::MISMATCH &&
                        getCodonPos(length(contigSeq), mutationsDNA[i + skip].posDNA, contigInfo.frame, contigInfo.reversed) != codonPos)
                {
                    break;
                }
                i += (skip - 1);
            }
        }
    }
}

template<typename TContigSeq, typename TContigInfo, typename TSingleAlleleStats>
int evaluateMutations(TSingleAlleleStats & allele,
                      const seqan::String<MutationDNA> mutationsDNA,
                      const TContigSeq & contigSeq,
                      const TContigInfo & contigInfo,
                      const KnownSnps<CardSnpRecord> & knownSnps)
{
    typedef KnownSnps<CardSnpRecord>::TSnpIter TSnpIter;

    TSnpIter tmpIt, tmp2It;
    size_t numKnownSnps = knownSnps.getSnpsForContig(tmpIt, tmp2It, allele.contigId);
    if(numKnownSnps && contigInfo.frame == static_cast<decltype(contigInfo.frame)>(-1))
    {
        return 0; //this gene was detected as invalid -> we cannot deduce mutations!
    }

    getObserverdMutations(allele, mutationsDNA, contigSeq, contigInfo, knownSnps);

    //create a map containing the observed amino acid mutations to be scanned in the known snps
    std::map<std::pair<std::pair<seqan::AminoAcid, seqan::AminoAcid>, size_t>, size_t> aaMutDict;
    for (size_t i = 0; i < length(allele.mutations); ++i)
    {
        const auto & mutAA = allele.mutations[i];
        if(mutAA.fromAA != mutAA.toAA)
        {
            //record non silent mutation
            aaMutDict[std::make_pair(std::make_pair(mutAA.fromAA, mutAA.toAA), mutAA.posAA)] = i;
        }
    }

    //search annotated mutations
    TSnpIter beginIt, endIt;
    bool conferred = false;
    size_t numSnps = knownSnps.getSnpsForContig(beginIt, endIt, allele.contigId);
    if(numSnps == 0)
    {
        //if no known snps for gene -> quit and return true
        //std::cerr << "no knowns snps\n";
        return true;
    }

    for(auto it1 = beginIt; it1 != endIt; ++it1)
    {
        bool allSnpsExist = true;
        const auto & mutations = it1->second.mutations;
        std::set<size_t> candMutBuffer;
        for (auto it2 = begin(mutations); it2 != end(mutations); ++it2)
        {
            size_t nucPos = getCodonPosFromAAPos(length(contigSeq), it2->pos, contigInfo.frame, contigInfo.reversed);
            //for sanity check also get the reference amino acid //TODO: at parsing
            seqan::String<seqan::AminoAcid> aaRef;
            if(!contigInfo.reversed)
                seqan::translate(aaRef, seqan::infix(contigSeq, nucPos, nucPos+3));
            else
                seqan::translate(aaRef, seqan::reverseComplementString(seqan::infix(contigSeq, nucPos, nucPos+3)));

            if(aaRef[0] != it2->from)
            {
                std::cerr << "Error reference base and snp \"from\" base mismatch!\n" << aaRef[0] << " vs. " << it2->from << '\t' << it2->pos << '\n';
                std::cerr << it1->second.accession << '\t' << nucPos << '\t' << length(contigSeq) << '\t' <<  it2->pos << '\t' << contigInfo.frame << '\t' << contigInfo.reversed << '\n';
                allSnpsExist = false;
                break;
            }
            auto needleIt = aaMutDict.find(std::make_pair(std::make_pair(it2->from, it2->to), it2->pos));

            if(needleIt == aaMutDict.end()) //check if mutation exists
            {
                allSnpsExist = false;
            }
            else
            {
                //buffer this known mutation as a candidate known snp (could be part of a multiple resistance variant)
                candMutBuffer.insert(needleIt->second);
            }
        }
        if (allSnpsExist) // we found one/multiple res. conferring mutations
        {
            for (const auto idx : candMutBuffer)
            {
                //mark this mutation as a known SNP (on AA level)
                allele.mutations[idx].isKnown = true;
            }
            conferred = true;
        }
    }
    return conferred;
}


//template<typename TContigSeq, typename TContigInfo, typename TSingleAlleleStats>
//int evaluateMutations(TSingleAlleleStats & allele,
//                      const seqan::String<MutationDNA> mutationsDNA,
//                      const TContigSeq & contigSeq,
//                      const TContigInfo & contigInfo,
//                      const KnownSnps<CardSnpRecord> & knownSnps)
//{
//    typedef KnownSnps<CardSnpRecord>::TSnpIter TSnpIter;

//    TSnpIter tmpIt, tmp2It;
//    size_t numKnownSnps = knownSnps.getSnpsForContig(tmpIt, tmp2It, allele.contigId);
////    if(numKnownSnps)
////        std::cerr <<"Acc:" << tmpIt->second.accession << '\t' << contigInfo.frame << '\t' << contigInfo.reversed << "\n\n";
////    else
////        std::cerr << "WHY?\n\n";

//    if(numKnownSnps && contigInfo.frame == static_cast<decltype(contigInfo.frame)>(-1))
//    {
//        return 0; //this gene was detected as invalid -> we cannot deduce mutations!
//    }

//    std::map<std::pair<std::pair<seqan::AminoAcid, seqan::AminoAcid>, size_t>, size_t> aaMutDict;
//    size_t winLen = 0;
//    for(size_t i = 0; i < seqan::length(mutationsDNA); ++i)
//    {
//        if (mutationsDNA[i].mType == MutationDNA::MISMATCH)
//        {
////            std::cerr << "mut: " << mutationsDNA[i].posDNA << " " << mutationsDNA[i].fromDNA
////                      << " " << mutationsDNA[i].toDNA << '\n';
//            size_t aaPos = getAAPos(length(contigSeq), mutationsDNA[i].posDNA, contigInfo.frame, contigInfo.reversed);
//            size_t codonPos = getCodonPos(length(contigSeq), mutationsDNA[i].posDNA, contigInfo.frame, contigInfo.reversed);
////            std::cerr << "! " << length(contigSeq) << '\t' << mutationsDNA[i].posDNA << '\t' << contigInfo.frame << '\t' << contigInfo.reversed << '\n';

//            if(aaPos == static_cast<decltype(aaPos)>(-1))
//            {
//                std::cerr << "Warning: Skipping invalid aaPos. Probably due to invalid gene!\n";
//                continue;
//            }
//            size_t nextAAPos = -1;
//            size_t nextCodonPos = -1;
//            if (i < seqan::length(mutationsDNA) - 1)
//            {
//                nextAAPos = getAAPos(length(contigSeq), mutationsDNA[i+1].posDNA, contigInfo.frame, contigInfo.reversed);
//                nextCodonPos = getCodonPos(length(contigSeq), mutationsDNA[i+1].posDNA, contigInfo.frame, contigInfo.reversed);
//            }

//            if (aaPos == nextAAPos)
//            {
//                assert(codonPos == nextCodonPos);
//                ++winLen;
//            }
//            else
//            {
//                assert(codonPos != nextCodonPos);
//                //size_t codonPos = getCodonPos(length(contigSeq), mutationsDNA[i].posDNA, contigInfo.frame, contigInfo.reversed);
//                if(codonPos == static_cast<decltype(codonPos)>(-1))
//                {
//                    std::cerr << "Warning: Skipping invalid codonPos. Probably due to invalid gene!\n";
//                    std::cerr << length(contigSeq) << '\t' <<  mutationsDNA[i].posDNA << '\t'
//                              << contigInfo.frame << '\t' << contigInfo.reversed << '\n';
//                    continue;
//                }

//                seqan::Dna5String triplet(seqan::infix(contigSeq, codonPos, codonPos + 3));
//                for(size_t w = 0; w <= winLen; ++w)
//                {
//                    triplet[mutationsDNA[i - w].posDNA - codonPos] = mutationsDNA[i - w].toDNA;
//                }

//                seqan::String<seqan::AminoAcid> aaMut, aaOrig;
//                if(contigInfo.reversed)
//                {
//                    seqan::translate(aaOrig, seqan::reverseComplementString(seqan::infix(contigSeq, codonPos, codonPos + 3)));
//                    seqan::reverseComplement(triplet);
//                    seqan::translate(aaMut, triplet);
////                    size_t len = length(contigSeq);
////                    std::cerr << len << '\t' << codonPos << '\t' << seqan::infix(contigSeq, codonPos, codonPos + 3) << '\t' << triplet << " !\n";
////                    std::cerr << aaPos << '\t' << aaOrig << '\t' << aaMut << '\n';
//                }
//                else
//                {
//                    seqan::translate(aaOrig, seqan::infix(contigSeq, codonPos, codonPos + 3));
//                    seqan::translate(aaMut, triplet);
////                    size_t len = length(contigSeq);
////                    std::cerr << len << '\t' << codonPos << '\t' << seqan::infix(contigSeq, codonPos, codonPos + 3) << '\t' << triplet << '\n';
//                }

////                std::cerr << "trip:" << triplet << '\n';
//                //seqan::translate(aaMut, triplet);
//                MutationAA mutAA;
//                mutAA.fromAA = aaOrig[0];
//                mutAA.toAA = aaMut[0];
//                mutAA.posAA = aaPos;
//                //append the associated DNA mutations
//                seqan::append(mutAA.mutationsDNA, seqan::infix(mutationsDNA, i-winLen,i+1));
//                seqan::appendValue(allele.mutations, mutAA);

////                std::cout << seqan::infix(contigSeq, codonPos, codonPos + 3) << " -> " << triplet << " " << aaOrig[0] << " -> " << aaMut[0] << '\n';
//                if(mutAA.fromAA != mutAA.toAA)
//                {
//                    //record non silent mutation
//                    aaMutDict[std::make_pair(std::make_pair(mutAA.fromAA, mutAA.toAA), aaPos)] = length(allele.mutations) - 1;
//                }
//                //reset window length
//                winLen = 0;
//            }
//        }
//    }

//    //Handle annotated mutations
//    TSnpIter beginIt, endIt;
//    bool conferred = false;
//    size_t numSnps = knownSnps.getSnpsForContig(beginIt, endIt, allele.contigId);
//    if(numSnps == 0)
//    {
//        //if no known snps for gene -> quit and return true
//        std::cerr << "no knowns snps\n";
//        return true;
//    }

//    for(auto it1 = beginIt; it1 != endIt; ++it1)
//    {
//        bool allSnpsExist = true;
//        const auto & mutations = it1->second.mutations;
//        std::set<size_t> candMutBuffer;
//        for (auto it2 = begin(mutations); it2 != end(mutations); ++it2)
//        {
//            //size_t nucPos = (it2->pos - 1) * 3; //todo check whether in snp file counting starts at 1 or 0
////            std::cerr <<"search mutation: " << it2->pos << '\t' << it2->from << '\t' << it2->to << '\n';
//            size_t nucPos = getCodonPosFromAAPos(length(contigSeq), it2->pos, contigInfo.frame, contigInfo.reversed);

//            //for sanity check also get the reference amino acid //TODO: at parsing
//            seqan::String<seqan::AminoAcid> aaRef;
//            if(!contigInfo.reversed)
//                seqan::translate(aaRef, seqan::infix(contigSeq, nucPos, nucPos+3));
//            else
//                seqan::translate(aaRef, seqan::reverseComplementString(seqan::infix(contigSeq, nucPos, nucPos+3)));

//            if(aaRef[0] != it2->from)
//            {
//                std::cerr << "Error reference base and snp \"from\" base mismatch!\n" << aaRef[0] << " vs. " << it2->from << '\t' << it2->pos << '\n';
//                std::cerr << it1->second.accession << '\t' << nucPos << '\t' << length(contigSeq) << '\t' <<  it2->pos << '\t' << contigInfo.frame << '\t' << contigInfo.reversed << '\n';
//                allSnpsExist = false;
//                break;
//                //exit(1);
//            }
//            auto needleIt = aaMutDict.find(std::make_pair(std::make_pair(it2->from, it2->to), it2->pos));

//            if(needleIt == aaMutDict.end()) //check if mutation exists
//            {
//                allSnpsExist = false;
//            }
//            else
//            {
//                //buffer this known mutation as a candidate known snp (could be part of a multiple resistance variant)
//                candMutBuffer.insert(needleIt->second);
//            }
//        }
//        if (allSnpsExist) // we found one/multiple res. conferring mutations
//        {
//            for (const auto idx : candMutBuffer)
//            {
//                //mark this mutation as a known SNP (on AA level)
//                allele.mutations[idx].isKnown = true;
//            }
//            conferred = true;

//        }
//    }
//    return conferred;
//}


//calculate the score for each allele according to the computed mapping profiles
//we use the same scoring as SRST2
template <typename TContigSeq, typename TContigInfo, typename TSnpRecordType>
void calcScore(SingleAlleleStats & allele,
               const TContigSeq & contigSeq,
               const TContigInfo & contigInfo,
               const KnownSnps<TSnpRecordType> & knownSnps,
               const Options & opts)
{    
    using seqan::String;
    const double errorProb = 0.01; //TODO: as param
    const size_t contigLen = length(contigSeq);

    //check if this gene requires snps. If it does and snps are missing -> stop
//    seqan::String<bool> snpNucPos;
//    if(!evaluateSnp(snpNucPos, allele, contigSeq, knownSnps))
//    {
//        //now conferring snps were found -> ignore this contig
//        return;
//    }

    //prepare the expected pvalues and the match/mismatch counts, as we use the same container for all.
    String<double> expectedPvals, weightedPVals;
    resize(expectedPvals, 2 * contigLen, 0.); //will be large enough to hold also the insertion penalties!
    resize(weightedPVals, 2 * contigLen, 0.);

    String<unsigned> matches, nonMatches;
    resize(matches, contigLen, 0);
    resize(nonMatches, contigLen, 0);

    //compute the number of matching and mismatching bases per position
    //mismatches also include deletions
    const String<unsigned> & insertionsRef = allele.profile[6];

    //the maximum depth of all bases for that allele (match/mismatch + insertions!)
    unsigned maxDepth = 0;
    unsigned cumulDepth = 0;
    unsigned depth5Prime = 0;
    unsigned depth3Prime = 0;
    unsigned insertionsTotal = 0;
    unsigned deletionsTotal = 0;
    unsigned mismatchesTotal = 0;
    unsigned coveredPos = 0;

    String<MutationDNA> mutations;
    for (size_t pos = 0; pos < contigLen; ++pos)
    {
        size_t refBase = static_cast<seqan::Dna5>(contigSeq[pos]).value;
        matches[pos] = allele.profile[refBase][pos];
        unsigned deletionsPos = allele.profile[5][pos];
        unsigned mismatchesPos = 0;

        size_t maxNucSupport = 0;
        seqan::Dna5 maxSupportedNuc = -1;
        for (size_t i = 0; i < 5; ++i)
        {
            if (i != refBase)
                mismatchesPos += allele.profile[i][pos];
            if(allele.profile[i][pos] > maxNucSupport)
            {
                maxSupportedNuc = i;
                maxNucSupport = allele.profile[i][pos];
            }
        }

        const unsigned depthPos = matches[pos] + mismatchesPos;
        if (maxNucSupport < depthPos / 2)
        {
            maxSupportedNuc = 4; //'N'
        }

        if (depthPos)
        {            
            maxDepth = std::max(maxDepth, depthPos);
            ++coveredPos;
        }

        nonMatches[pos] = mismatchesPos + deletionsPos;
        //std::cerr << allele.locusId << " " << contigLen << " " << length(allele.profile[0]) << " " << pos << " " << matches[pos] << " " << nonMatches[pos] << " " << insertionsRef[pos] << std::endl;
        if (deletionsPos > matches[pos])
        {
            MutationDNA m;
            m.mType = MutationDNA::DELETION;
            m.posDNA = pos;
            m.fromDNA = refBase;
            seqan::appendValue(mutations, m);
            ++deletionsTotal;
        }
        if (mismatchesPos > matches[pos])
        {
            MutationDNA m;
            m.mType = MutationDNA::MISMATCH;
            m.posDNA = pos;
            m.fromDNA = refBase;
            m.toDNA = maxSupportedNuc;
            seqan::appendValue(mutations, m);
            ++mismatchesTotal;
        }
        if(insertionsRef[pos] > depthPos/2.)
        {
            MutationDNA m;
            m.mType = MutationDNA::INSERTION;
            m.posDNA = pos; //actually no real handling of insertions
            seqan::appendValue(mutations, m);
            ++insertionsTotal;
        }

        if (pos < opts.terminalLength)
            depth5Prime += depthPos;
        if (pos >= contigLen - opts.terminalLength)
            depth3Prime += depthPos;

        cumulDepth += depthPos;
    }

    allele.numInsertions = insertionsTotal;
    allele.numDeletions = deletionsTotal;
    allele.numMismatches = mismatchesTotal;
    allele.divergence = static_cast<double>(mismatchesTotal + deletionsTotal) / coveredPos;
    allele.avgDepth = static_cast<double>(cumulDepth) / coveredPos;
    allele.avgDepth5Prime = static_cast<double>(depth5Prime) / opts.terminalLength;
    allele.avgDepth3Prime = static_cast<double>(depth3Prime) / opts.terminalLength;
    allele.coverage = static_cast<double>(coveredPos) / contigLen;

    if (opts.mode >= Options::RESISTANCE_GENES_HOM)
    {
        bool knownSnpsPresent = evaluateMutations(allele, mutations, contigSeq, contigInfo, knownSnps);

        //correct the number of matches / non matches
        if(knownSnpsPresent)
        {
            for (const auto & m : allele.mutations)
            {
                if (m.isKnown)
                {
                    allele.numMismatches -= length(m.mutationsDNA);
                }
            }
        }
        allele.divergence = static_cast<double>(allele.numMismatches + allele.numDeletions) / coveredPos;

        if (!knownSnpsPresent)
            allele.issues.set(SingleAlleleStats::MISSING_SNPS); //ONLY important for variable snp mode
    }
    else //MLST Mode - just record the nucleotide mutations
    {
        MutationAA tmp; // = {'*', '*', -1, mutations, false};
        tmp.mutationsDNA = mutations;
        seqan::appendValue(allele.mutations, std::move(tmp));
    }
    //order mutations by position
    std::sort(begin(allele.mutations), end(allele.mutations), [](auto l, auto r){return l.posAA < r.posAA;});

    //add penalty nonMatch counts for uncovered regions
    penalizeGaps(nonMatches, matches, false);

    //get weighted pvalues (weighted by relative base coverage)
    size_t numPVals = 0;
    for (size_t i = 0; i < contigLen; ++i)
    {
        const unsigned depth = matches[i] + nonMatches[i];
        //binomial distribution for score calculation
        if (depth > 0)
        {
            boost::math::binomial bino(depth, 1.0 - errorProb);
            double pVal = boost::math::cdf(bino, matches[i]);
            if (pVal > 0)
                weightedPVals[numPVals++] = -log10(pVal * static_cast<double>(depth) / maxDepth);
            else
                weightedPVals[numPVals++] = 1000;

//          std::cout << i << '\t' << depth << '\t' << matches[i] << '\t' << pVal << '\t' << weightedPVals[numPVals-1] << '\n';
        }
        //handle insertions
        if (insertionsRef[i] > 0)
        {
            const unsigned covWithInserts = depth + insertionsRef[i];
            boost::math::binomial bino(covWithInserts, 1.0 - errorProb);
//            const unsigned covWithInserts = depth;// + insertionsRef[i];
//            boost::math::binomial bino(covWithInserts, 1.0 - errorProb);
            double pVal = boost::math::cdf(bino, depth);
            if (pVal > 0)
                weightedPVals[numPVals++] = -log10(pVal * static_cast<double>(covWithInserts) / maxDepth);
            else
                weightedPVals[numPVals++] = 1000;
        }
    }

    //generate expected distribution in the predefined container
    unsigned generatorIndex = numPVals;
    std::generate_n(begin(expectedPvals), numPVals, [&generatorIndex, numPVals](){return -log10(generatorIndex--/static_cast<double>(numPVals+1));});

    //sort the log transformed values and compare against expected distribution
    std::sort(begin(weightedPVals), begin(weightedPVals) + numPVals);
#ifdef DEBUG
    for (size_t i = 0; i < numPVals; ++i)
        std::cout << expectedPvals[i] << ' ';
    std::cout << '\n';
    for (size_t i = 0; i < numPVals; ++i)
        std::cout << weightedPVals[i] << ' ';
    std::cout << '\n';
#endif

    seqan::Pair<double, double> slopeIntercept = computeRegressionN(expectedPvals, weightedPVals, numPVals, true);
    //        for(size_t kk = 0; kk < numPVals; ++kk)
    //            std::cout << expectedPvals[kk] << " vs " << weightedPVals[kk] << '\n';
#ifdef DEBUG
    std::cout << "Num Pvals: " << numPVals << " slope: " << slopeIntercept.first << '\n';
#endif
    allele.score = slopeIntercept.i1;
}

template <typename TContigStore, typename TContigInfos, typename TLocusAlleleId, typename TSnpRecordType>
void calcAllelStats(seqan::String<SingleAlleleStats> & bestAlleles,
                    seqan::BamFileIn & bamfile,
                    const TContigStore & contigStore,
                    const TContigInfos & contigInfos,
                    const seqan::String<TLocusAlleleId> & contigToAllele,
                    const KnownSnps<TSnpRecordType> & knownSnps,
                    const Options & opts)
{
    //parse the bam file and adjust the profiles for the corresponding allele
    seqan::BamHeader header;
    readHeader(header, bamfile);
    seqan::BamAlignmentRecord record;

    SingleAlleleStats allele;

    decltype(record.rID) prev_rID = -1;
    while(!atEnd(bamfile))
    {
        readRecord(record, bamfile);

        if (record.beginPos == record.INVALID_POS) //skip unaligned read
            continue;
//        if (!hasFlagAllProper(record)) //skip non properly aligned reads/pairs
//            continue;
        if(record.mapQ < opts.minMappingScore) //skip low scoring alignment
            continue;
        if(contigToAllele[record.rID].i1 == static_cast<decltype(contigToAllele[record.rID].i1)>(-1))
            continue; //invalid allele (too long / short)

        if (record.rID != prev_rID)
        {
            //done with contig/allele - check whether it is a top scorer and refresh allele object
            //calculate score
            if (prev_rID != -1)
            {
                calcScore(allele, contigStore[prev_rID].seq, contigInfos[prev_rID], knownSnps, opts);
                updateTopLocusAlleles(bestAlleles,allele,opts);
            }
            //refresh allele object
            initAlleleStat(contigStore[record.rID].seq, contigToAllele[record.rID], allele);
            allele.contigId = record.rID;
            prev_rID = record.rID;
        }

        SingleAlleleStats::TProfile & profile = allele.profile;

        //parse the cigar string
        //get endpos of read for calculation of unmatched overlaps
        decltype(record.beginPos) readPos = 0;
        auto contigPos = record.beginPos;
        for (const auto & cigarElem : record.cigar)
        {
            if(cigarElem.operation == 'M' || cigarElem.operation == '=' || cigarElem.operation == 'X') //count (mis)matched character
            {
                readPos += cigarElem.count;
                contigPos += cigarElem.count;
            }
            else if (cigarElem.operation == 'D' || cigarElem.operation == 'N') //count gap character
            {
                contigPos += cigarElem.count;
            }
            else if(cigarElem.operation == 'I' || cigarElem.operation == 'S') //skip read positions
            {
                readPos += cigarElem.count;
            }
        }
        std::pair<unsigned,unsigned> unmatchedOverlaps = getUnmatchedOverlaps(record.cigar, record.beginPos, contigPos, length(contigStore[record.rID].seq));

        if(unmatchedOverlaps.first > opts.maxUnmatchedOverlaps  || unmatchedOverlaps.second > opts.maxUnmatchedOverlaps)
        {
            //std::cerr << "Skipping read due to large overlaps!\n";
            continue;
            //todo write to logfile
        }

        //now parse cigar once again to fill the profiles
        readPos = 0;
        contigPos = record.beginPos;
        for (const auto & cigarElem : record.cigar)
        {
            if(cigarElem.operation == 'M' || cigarElem.operation == '=' || cigarElem.operation == 'X') //count (mis)matched character
            {
                for (size_t i = 0; i < cigarElem.count; ++i, ++readPos, ++contigPos)
                {
                    if(record.qual[readPos] >= opts.minPhredScore + 33)
                    {
                        ++profile[static_cast<seqan::Dna5>(record.seq[readPos]).value][contigPos];
                        //std::cout << profile[static_cast<Dna5>(record.seq[readPos]).value][contigPos] << '\n';
                    }
#ifdef DEBUG
                    else
                        std::cerr << "skipping due to low Phred score " << contigPos << '\n';
#endif
                }
            }
            else if (cigarElem.operation == 'D' || cigarElem.operation == 'N') //count gap character
            {
                for (size_t i = 0; i < cigarElem.count; ++i, ++contigPos)
                    ++profile[5][contigPos];
            }
            else if(cigarElem.operation == 'I') //add single penalty for insertion
            {
                for (size_t i = 0; i < cigarElem.count; ++i)
                {
                    if(record.qual[readPos + i] >= opts.minPhredScore + 33)
                    {
                        ++profile[6][contigPos];
#ifdef DEBUG
                        std::cout << "Insertion at: " << contigPos << '\n';
#endif
                        break;
                    }
                }
                readPos += cigarElem.count;
            }
            else if(cigarElem.operation == 'S') //skip read positions
            {
                readPos += cigarElem.count;
            }
        }
    }
    //handle last contig
    if (prev_rID != -1)
    {
        calcScore(allele, contigStore[prev_rID].seq, contigInfos[prev_rID], knownSnps, opts);
        updateTopLocusAlleles(bestAlleles,allele,opts);
    }
}

template <typename TContigStore, typename TLocusAlleleId>
void calcAllelStats(seqan::String<SingleAlleleStats> & bestAlleles,
                    seqan::BamFileIn & bamfile,
                    const TContigStore & contigStore,
                    const seqan::String<TLocusAlleleId> & contigToAllele,
                    const Options & opts)
{

    seqan::String<ContigGeneInfo> dummyInfo;
    seqan::resize(dummyInfo, length(contigStore));
    return calcAllelStats(bestAlleles, bamfile, contigStore, dummyInfo, contigToAllele, KnownSnps<CardSnpRecord>(), opts);
}

#endif // MAPCOUNTING_H
