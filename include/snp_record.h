//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#ifndef SNP_RECORD_H
#define SNP_RECORD_H

#include <seqan/basic.h>
#include <seqan/sequence.h>
#include <seqan/stream.h>

struct CardSnpRecord
{
    typedef unsigned TAccessionType;
    static TAccessionType invalidID(){return -1;}

    enum MutationType
    {
        CO_DEPENDENT_SINGLE_RESISTANCE_VARIANT,
        FRAMESHIFT_MUTATION,
        MULTIPLE_RESISTANCE_VARIANTS,
        NONSENSE_MUTATION,
        SINGLE_RESISTANCE_VARIANT
    };

    typedef seqan::Position<seqan::String<seqan::AminoAcid>>::Type TPos;

    struct Mutation
    {
        TPos pos = -1;
        seqan::AminoAcid from;
        seqan::AminoAcid to;
    };

    TAccessionType accession = -1;
    seqan::CharString name;
    seqan::CharString modelType;
    MutationType paramType;
    seqan::String<Mutation> mutations;
};

template<typename TRecordType>
struct KnownSnps
{
    typedef std::multimap<size_t, TRecordType> TMapType;
    typedef typename TMapType::const_iterator TSnpIter;

    size_t getSnpsForContig(TSnpIter & beginIt, TSnpIter & endIt, size_t contigId) const
    {
        auto range = knownSnps.equal_range(contigId);
        beginIt = range.first;
        endIt = range.second;

        return knownSnps.count(contigId);
    }
    TMapType knownSnps;
};

inline bool geneNameToAccessionType(CardSnpRecord::TAccessionType & acc, const seqan::CharString & geneName, const CardSnpRecord & /*tag*/)
{
    seqan::StringSet<seqan::CharString> tokens, tokens_id;
    seqan::strSplit(tokens, geneName, seqan::EqualsChar<'|'>());
    if (seqan::length(tokens) != 6)
    {
        std::cerr << "cannot extract accession from gene Name: " << geneName << '\n';
        return false;
    }
    seqan::strSplit(tokens_id, tokens[4], seqan::EqualsChar<':'>());
    if ( (seqan::length(tokens_id) != 2) | (tokens_id[0] != "ARO"))
    {
        std::cerr << "Invalid accession. Exprected ARO:<acc> : " << tokens[4] << '\n';
        return false;
    }

    //std::cerr << "L: " << seqan::length(tokens) << '\n' << geneName << '\n';
    std::istringstream ss(seqan::toCString(tokens_id[1]));

    ss >> acc;
    return true;
}

#endif // SNP_RECORD_H
