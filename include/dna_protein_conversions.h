//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#ifndef DNAPROTEINCONVERSIONHELPER_H
#define DNAPROTEINCONVERSIONHELPER_H

#include <seqan/basic.h>
#include <seqan/sequence.h>
#include <seqan/modifier.h>
#include <seqan/translation.h>

inline size_t getAAPos(size_t contigLen, size_t posDNA, size_t frame, bool reverse)
{
    if (posDNA >= contigLen)
        return -1; //position outside contig

    if (!reverse)
    {
        if (posDNA < frame)
            return -1; //position outside orf -> no aaPos
        return (posDNA - frame) / 3;
    }
    else
    {
        if (contigLen - posDNA - 1 < frame)
            return -1; //position outside orf -> no aaPos
        return (contigLen - posDNA - 1 - frame) / 3;
    }
}

inline size_t getCodonPos(size_t contigLen, size_t posDNA, size_t frame, bool reverse)
{
    if (posDNA >= contigLen)
        return -1; //position outside contig

    if (!reverse)
    {
        if (posDNA < frame)
            return -1; //position outside orf -> no codonPos
        const size_t posCodon = posDNA - ((posDNA - (frame % 3)) % 3);
        if (posCodon + 3 > contigLen)
            return -1;
        return posCodon;
    }
    else
    {
        size_t posDNAR = contigLen - 1 - posDNA;
        if (posDNAR < frame)
            return -1; //position outside orf -> no codonPos
        const size_t posCodonR = posDNAR - ((posDNAR - (frame % 3)) % 3);
        if (posCodonR + 3 > contigLen)
            return -1;
        return contigLen - 3 - posCodonR;
    }
}

inline size_t getCodonPosFromAAPos(size_t contigLen, size_t posAA, size_t frame, bool reverse)
{
    const size_t codonPos = 3 * posAA;

    if (codonPos + 2 + frame >= contigLen)
        return -1; //position outside contig

    if (!reverse)
    {
        return codonPos + frame;
    }
    else
    {
        //-1 (for length offset)
        //-2 (to get first position on available strand,
        //  i.e. last codon position w.r.t. coding sequence)
        return contigLen - codonPos - 3 - frame;
    }
}

template<typename TDnaString>
inline seqan::AminoAcid getAAAtPos(const TDnaString & contigSeq, size_t posAA, size_t frame, bool reverse)
{
    size_t contigLen = length(contigSeq);
    size_t codonPos = getCodonPosFromAAPos(contigLen, posAA, frame, reverse);

    if (codonPos == static_cast<decltype(codonPos)>(-1))
        return 'X'; //marks invalid call

    seqan::String<seqan::AminoAcid> aa;

    if(reverse)
        seqan::translate(aa, seqan::reverseComplementString(seqan::infix(contigSeq, codonPos, codonPos + 3)));
    else
        seqan::translate(aa, seqan::infix(contigSeq, codonPos, codonPos + 3));

    return aa[0];
}

#endif // DNAPROTEINCONVERSIONHELPER_H
