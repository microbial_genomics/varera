//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#include <mapcounting.h>
#include <snp_file.h>
#include <contig_loading.h>
#include <reporting.h>

//std includes
#include <iostream>
#include <bitset>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>

//seqan includes
#include <seqan/basic.h>
#include <seqan/sequence.h>
#include <seqan/stream.h>
#include <seqan/bam_io.h>
#include <seqan/store.h>
#include <seqan/arg_parse.h>
#include <seqan/stream.h>
#include <seqan/index.h>

//#define DEBUG

using namespace seqan;
//typedef FragmentStore<void, MyFragStoreConfig<void>> TFragmentStore;
//typedef TFragmentStore::TContigStore TContigStore;
//typedef TFragmentStore::TContigNameStore TContigNameStore;



typedef String<SingleAlleleStats> TAlleleStats;
typedef String<ContigType> TContigTypeList;


seqan::ArgumentParser::ParseResult
parseCommandLine(Options & options, int argc, char const ** argv)
{
    // Setup ArgumentParser.
    seqan::ArgumentParser parser("ReMatch");
    seqan::setVersion(parser, "0.1");
    seqan::setDate(parser, "August 2017");

    addOption(parser, seqan::ArgParseOption(
                  "U", "uppercase", "Select to-uppercase as operation.", seqan::ArgParseArgument::INTEGER, "INT"));
    setMinValue(parser, "uppercase", "0");
    setMaxValue(parser, "uppercase", "10");

    addSection(parser, "INPUT/OUTPUT FILES");
    //alleles fasta file
    seqan::addOption(parser, seqan::ArgParseOption(
                         "g", "input-genes", "Path to the input file with allele sequences",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "input-genes", "fa fasta");
    setRequired(parser, "g");

    //mapped reads sam/bam
    seqan::addOption(parser, seqan::ArgParseOption(
                         "ra", "input-reads-aligned", "Path to the input file with aligned reads. This turns on the mapping based mode! If not given \"input-reads\" must be provided!",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "input-reads-aligned", "bam sam");

    //alleles fasta file
    seqan::addOption(parser, seqan::ArgParseOption(
                         "gc", "gene-clusters", "Path to the input file with grouping of genes to clusters",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "gene-clusters", "txt csv");

    //snps file csv
    seqan::addOption(parser, seqan::ArgParseOption(
                         "snp", "snp-file", "Path to the input file with known snps (currently expecting card format)",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "snp-file", "tsv csv");

    //output file
    seqan::addOption(parser, seqan::ArgParseOption(
                         "o", "output-file-summary", "Path to the output file with a summary of the identification run",
                         seqan::ArgParseArgument::OUTPUT_FILE, "OUT"));
    setValidValues(parser, "output-file-summary", "csv");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "c", "output-file-cov", "Path to the output file with coverage details used to create figures in report",
                         seqan::ArgParseArgument::OUTPUT_FILE, "OUT"));
    setValidValues(parser, "output-file-cov", "csv");

    addSection(parser, "Filtering Options");
    seqan::addOption(parser, seqan::ArgParseOption(
                         "ad", "min-average-depth", "minimal average read depth for a gene to be reported",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-average-depth", 5);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "adw", "min-average-depth-warn", "minimal average read depth for a gene (otherwise a warning will be reported)",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-average-depth-warn", 10);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "mc", "min-coverage", "minimal fraction of a gene to be covered by read alignments",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "min-coverage", 0.9);
    setMinValue(parser, "min-coverage", "0.1");
    setMaxValue(parser, "min-coverage", "1.0");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "mcw", "min-coverage-warn", "minimal fraction of a gene to be covered by read alignments",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "min-coverage-warn", 0.95);
    setMinValue(parser, "min-coverage-warn", "0.1");
    setMaxValue(parser, "min-coverage-warn", "1.0");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "md", "max-divergence", "maximal allowed divergence for a gene to be regarded as a match",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "max-divergence", 0.02);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "mdw", "max-divergence-warn", "maximal allowed divergence for a gene to be regarded as a match",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "max-divergence-warn", 0.01);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "td", "min-terminal-depth", "minimal depth required for the terminal regions of the gene (otherwise a warning will be reported)",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-terminal-depth", 2);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "tl", "terminal-length", "length of the terminal regions (5' and 3') of the gene. Important for option \"min-terminal-depth\"",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "terminal-length", 2);

    addSection(parser, "Read Filtering Options");
    seqan::addOption(parser, seqan::ArgParseOption(
                         "ms", "min-mapping-score", "minimal mapping score for aligned reads",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-mapping-score", 1);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "bs", "min-base-score", "minimal Phred score for aligned bases",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-base-score", 20);
    setMinValue(parser, "min-base-score", "0");
    setMaxValue(parser, "min-base-score", "60");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "uo", "max-unaligned-overlap", "maximal length of clipped bases overlapping the gene (not at the 5' or 3' end)",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "max-unaligned-overlap", 20);

    // Parse command line.
    seqan::ArgumentParser::ParseResult res = seqan::parse(parser, argc, argv);

    // Only extract  options if the program will continue after parseCommandLine()
    if (res != seqan::ArgumentParser::PARSE_OK)
        return res;

    // Extract option values.
    getOptionValue(options.maxUnmatchedOverlaps, parser, "max-unaligned-overlap");
    getOptionValue(options.terminalLength, parser, "terminal-length");
    getOptionValue(options.minTerminalDepth, parser, "min-terminal-depth");

    getOptionValue(options.minAvgDepth, parser, "min-average-depth");
    getOptionValue(options.minAvgDepthW, parser, "min-average-depth-warn");
    getOptionValue(options.minCoverage, parser, "min-coverage");
    getOptionValue(options.minCoverageW, parser, "min-coverage-warn");
    getOptionValue(options.maxDivergence, parser, "max-divergence");
    getOptionValue(options.maxDivergenceW, parser, "max-divergence-warn");

    unsigned tmpMinPhredScore;
    getOptionValue(tmpMinPhredScore, parser, "min-base-score");
    options.minPhredScore = tmpMinPhredScore;
    getOptionValue(options.minMappingScore, parser, "min-mapping-score");

    seqan::getOptionValue(options.bamFileName, parser, "input-reads-aligned");

    seqan::getOptionValue(options.alleleFileName, parser, "input-genes");
    if (isSet(parser, "gene-clusters"))
        seqan::getOptionValue(options.clusterFile, parser, "gene-clusters");
    if (isSet(parser, "snp-file"))
    {
        seqan::getOptionValue(options.snpFile, parser, "snp-file");
        options.mode = options.RESISTANCE_GENES_VAR;
    }
    else
    {
        options.mode = options.RESISTANCE_GENES_HOM;
    }
    if (isSet(parser, "output-file-summary"))
        seqan::getOptionValue(options.summaryOutFileName, parser, "output-file-summary");
    if (isSet(parser, "output-file-cov"))
        seqan::getOptionValue(options.covOutFileName, parser, "output-file-cov");

    return seqan::ArgumentParser::PARSE_OK;
}

int main(int argc, const char ** argv)
{
    typedef Pair<unsigned, unsigned> TLocusAlleleId;

    //-------------------------------
    //-----HANDLING COMMMAND LINE----
    //-------------------------------
    Options opts;
    seqan::ArgumentParser::ParseResult res = parseCommandLine(opts, argc, argv);

    // If parsing was not successful then exit with code 1 if there were errors.
    // Otherwise, exit with code 0 (e.g. help was printed).
    if (res != seqan::ArgumentParser::PARSE_OK)
        return res == seqan::ArgumentParser::PARSE_ERROR;

    //--------------------------------
    //--------------READ INPUT--------
    //--------------------------------
    TFragmentStoreContigs fragStore;
    String<ContigGeneInfo> contigInfos;
    String<TLocusAlleleId> contigToLocus;
    String<ContigHeaderDetail> contigDetails;
    TContigTypeList contigTypes;
    KnownSnps<CardSnpRecord> knownSnps;

    try
    {

        unsigned numClusters = loadContigData(fragStore, contigInfos, contigTypes, contigToLocus, opts);
        extractDetailedContigInfo(contigDetails, opts.alleleFileName);

        if (!validateMatchingFiles(fragStore.contigNameStore, opts.bamFileName))
        {
            std::cerr << "Non-matching reference names in fasta and bam file!\n";
            return 1;
        }

        //sanity check the detailed header infos were fetched for all entries
        if (length(contigDetails) != length(fragStore.contigNameStore))
        {
            std::cerr << "Internal error at parsing detailed fasta header information"
                         " Unequal lengths! Please contact developer!";
            return 1;
        }
        else
        {
            for(size_t c = 0; c < length(contigDetails); ++c)
            {
                if (!isPrefix(fragStore.contigNameStore[c], contigDetails[c].full))
                {
                    std::cerr << "Internal error at parsing detailed fasta header information.\n"
                                 "Header parsing naming conflict! Please contact developer!";
                    return 1;
                }
            }
        }

        // Open Bamfile
        BamFileIn bamFileIn;
        if (!open(bamFileIn, toCString(opts.bamFileName)))
        {
            std::cerr << "ERROR: Could not open " << opts.bamFileName << std::endl;
            return 1;
        }

        //--------------------------------
        //--load SNPs if provided
        //--------------------------------
        if(!seqan::empty(opts.snpFile))
        {
            readSnpFile(knownSnps, fragStore.contigNameStore, opts.snpFile);
        }

        //--------------------------------
        //-----------ANALYSIS-------------
        //--------------------------------

        //setup data structures holding the stats for each allle and the names of the loci
        TAlleleStats bestAlleles;
        resize(bestAlleles, length(contigToLocus));

        /*
     * temporary fix to get all alleles per group and
     * do grouping afterwards. @TODO: Refactor
     */
        decltype(contigToLocus) contigToLocusTmp;
        resize(contigToLocusTmp, length(contigToLocus));
        for (size_t c = 0; c < length(contigToLocusTmp); ++c)
        {
            contigToLocusTmp[c].i1 = c;
            contigToLocusTmp[c].i2 = 1;
        }


        //calculate stats for each allele
        calcAllelStats(bestAlleles, bamFileIn, fragStore.contigStore, contigInfos, contigToLocusTmp, knownSnps, opts);

#ifdef DEBUG
        for (int i = 0; i < length(alleleStats); ++i)
        {
            if (alleleStats[i].score  < 100)
                std::cout<< "avg cov " << fragStore.contigNameStore[i] << ": " << alleleStats[i].avgDepth << " Score: " << alleleStats[i].score << '\n';
        }
#endif

        //reset original groups of contigs
        for(SingleAlleleStats & all : bestAlleles)
        {
            if (all.contigId != -1)
            {
                all.locusId = contigToLocus[all.contigId].i1;
                if (contigInfos[all.contigId].frame == -1)
                    all.issues.set(SingleAlleleStats::INVALID_GENE);
            }
        }

        //sort alleles by group and score and number of issues
        std::sort(begin(bestAlleles), end(bestAlleles), [&contigToLocus](const SingleAlleleStats & a, const SingleAlleleStats & b){
            return (a.locusId < b.locusId ||
                    ((a.locusId == b.locusId) && (a.score < b.score)) ||
                    ((a.locusId == b.locusId) && (a.score == b.score) && (a.issues.count() < b.issues.count())));});

        //use full header line in coverage output
        String<CharString> contigNameStoreFull;//, contigNameStoreShortId;
        for(const auto & cd : contigDetails)
        {
            appendValue(contigNameStoreFull, cd.full);
            //appendValue(contigNameStoreShortId, cd.shortId);
        }

        //write files for all genes
        writeSummaryOutputResMatcher(bestAlleles, contigDetails, opts);
        writeCoverageOutput(bestAlleles, fragStore.contigStore, contigNameStoreFull, opts);

        //keep only the top scoring allele per locus
        decltype(bestAlleles) bestAllelesTop;
        unsigned prevLocus = -1;
        for(const auto & all : bestAlleles)
        {
            if(all.locusId != prevLocus)
            {
                appendValue(bestAllelesTop, all);
                prevLocus = all.locusId;
            }
        }

        std::regex reg ("\\.csv$");
        opts.summaryOutFileName = std::regex_replace (toCString(opts.summaryOutFileName), reg,"_top.csv");
        opts.covOutFileName = std::regex_replace (toCString(opts.covOutFileName), reg,"_top.csv");

        //if grouping was used write summary and coverage only for top hits per group
        writeSummaryOutputResMatcher(bestAllelesTop, contigDetails, opts);
        writeCoverageOutput(bestAllelesTop, fragStore.contigStore, contigNameStoreFull, opts);
    }
    catch (const std::exception & e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }

    return 0;
}

