//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

//std includes
#include <iostream>
#include <bitset>
#include <algorithm>
#include <unordered_map>

//seqan includes
#include <seqan/basic.h>
#include <seqan/sequence.h>
#include <seqan/stream.h>
#include <seqan/bam_io.h>
#include <seqan/store.h>
#include <seqan/arg_parse.h>
#include <seqan/stream.h>

//boost includes
#include <boost/random/binomial_distribution.hpp>
#include <boost/math/distributions/binomial.hpp>

//#define DEBUG
#define SRST2

//@TODO: calculate and use edge depth for filtering -- for mlst
//@TODO: calculate and use coverage for filtering --only for non mlst
//@TODO: use divergence for filtering -- only for non mlst

using namespace seqan;

struct SingleAlleleStats
{
    typedef StringSet<String<unsigned>> TProfile;

    unsigned locusId = -1;
    unsigned alleleId;

    double score = std::numeric_limits<double>::max();
    double avgDepth = 0.;   //average depth across complete allele
    double avgDepth5Prime = 0.; //average depth at alleles 5' end
    double avgDepth3Prime = 0.; //average depth at alleles 3' end
    double divergence = 0.; //fraction of non-matching positions
    double coverage = 0.; //fraction of allele covered

    unsigned numInsertions = 0;
    unsigned numDeletions = 0;
    unsigned mismatches = 0;


    TProfile profile;

    bool operator<(const SingleAlleleStats & rhs) const
    {
        return score < rhs.score;
    }

};

struct TSequenceTypeList
{
    StringSet<String<unsigned>> sequenceTypes;
    String<CharString> clonal_complex;
};

struct Options
{
    CharString bamFileName;
    CharString alleleFileName;
    CharString stFileName;
    CharString outFileName;

    char delim = '-';
    unsigned maxUnmatchedOverlaps = 10;
    unsigned terminalLength = 2;
    unsigned minTerminalDepth = 2;
    unsigned minAvgDepth = 2;
    double minCoverage = 0.9;
    char minPhredScore = 20;
    unsigned minMappingScore = 1;
};

typedef FragmentStore<> TFragmentStore;
typedef FragmentStore<>::TContigStore TContigStore;
typedef FragmentStore<>::TContigNameStore TContigNameStore;

typedef std::map<CharString, StringSet<Dna5String,Dependent<>>> TReferenceAlleles;
typedef String<SingleAlleleStats> TAlleleStats;


//calculate the regression between the first n elements of x and y
std::pair<double, double> computeRegressionN(const String<double> & x, const String<double> & y, size_t n)
{
        const double sx  = std::accumulate(begin(x), begin(x)+n, 0.0);
        const double sy  = std::accumulate(begin(y), begin(y)+n, 0.0);

        //sum_i(x_i * x_i)
        const double xx = std::inner_product(begin(x), begin(x)+n, begin(x), 0.0);
        //sum_i(x_i * y_i)
        const double xy = std::inner_product(begin(x), begin(x)+n, begin(y), 0.0);

        const double slope = ((n * xy) - (sx * sy)) / ((n * xx) - (sx * sx));
        const double intercept = (sy - slope * sx) / n;

        return std::make_pair(slope, intercept);
}


void loadData(TFragmentStore & fragStore, BamFileIn & bamFileIn, TSequenceTypeList & types, String<CharString> & locusNames, const Options & opts)
{
    //--------------------------------
    //--load allele sequences in contig store
    //--------------------------------
    loadContigs(fragStore, opts.alleleFileName);

    TContigNameStore contigNames = fragStore.contigNameStore;
    //Order of contigs in sam/bam should be the same as in reference fasta file

    CharString lastLocusName("");
    for (const auto & contig : contigNames)
    {
        std::string contigStd(toCString(contig)); //convert to string to use find_last_of
        size_t delimPos = contigStd.find_last_of(opts.delim);
        if (delimPos == std::string::npos)
        {
            std::cerr << "Invalid contig name! Expected delimiter \"" << opts.delim << "-\" but not found! " << contigStd<< '\n';
            exit(1); //todo exception
        }
        CharString locusName(contigStd.substr(0, delimPos));
        if (locusName != lastLocusName)
        {
            appendValue(locusNames, locusName);
            lastLocusName = locusName;
        }
    }

    //--------------------------------
    //--load sequence type information
    //--------------------------------
    if (!empty(opts.stFileName))
    {
        std::fstream stream(toCString(opts.stFileName), std::ios::binary | std::ios::in);
        if (!stream.good())
        {
            std::cerr << "Unable to open sequence type file " << opts.stFileName << '\n';
            exit(1);
        }

        //read header containing the locus names
        String<CharString> columnNames;
        std::string header, cell;
        std::getline(stream, header);
        std::istringstream headerStream(header);

        while(std::getline(headerStream,cell, '\t'))
            appendValue(columnNames, cell);

        String<unsigned> indexMap; //to capture the case that order csv file is not alphabetical
        resize(indexMap, length(columnNames) - 1); //-1 as we skip the clonal-complex column
        const unsigned clonalColId = length(columnNames) - 1; //column id of clonal complex info

        for (size_t i = 1; i < length(columnNames)-1; ++i)
        {
            const auto it = std::find(begin(locusNames), end(locusNames), columnNames[i]);
            if(it != end(locusNames))
                indexMap[i] =  it - begin(locusNames);
            else
            {
                std::cerr << "unknown locus id: " << columnNames[i] << " --ABORT\n";
                exit(1);
            }
        }

        //get the number of sequence types
        std::string maxSt, line;
        while(std::getline(stream, line))
        {
            std::istringstream lineStream(line);
            lineStream >> maxSt;
        }
        size_t numSeqTypes = std::stoi(maxSt); //assume that sequence types are sorted

        resize(types.sequenceTypes, numSeqTypes + 1);
        for (auto & s : types.sequenceTypes)
            resize(s, length(locusNames), -1);
        resize(types.clonal_complex, numSeqTypes + 1);

        stream.clear(); //reset stream
        stream.seekg(0, std::ios::beg);
        std::getline(stream, line); //skip header

        while(std::getline(stream, line))
        {
            std::istringstream lineStream(line);
            std::string cell;

            size_t colId = 0;
            size_t stNum = 0;
            while(std::getline(lineStream,cell, '\t'))
            {
                //std::cout << colId << '\t' << cell << '\n';
                if (colId == 0)
                {
                    stNum = stoi(cell);
                }
                else if (colId == clonalColId)
                {
                    types.clonal_complex[stNum] = cell;
                }
                else
                {
                    types.sequenceTypes[stNum][indexMap[colId]] = stoi(cell);
                }
                ++colId;
            }

        }
    }

    //--------------------------------
    //--load bam file
    //--------------------------------
    // Open input file, BamFileIn can read SAM and BAM files.
    if (!open(bamFileIn, toCString(opts.bamFileName)))
    {
        std::cerr << "ERROR: Could not open " << opts.bamFileName << std::endl;
        exit(1);
    }
}

//reverve the space required to store the mapping profiles for each allele
//assign locusIds and alleleIds
void setupAlleleStats(const TContigStore &contigStore, const TContigNameStore & names, const String<CharString> & locusNames, const Options & opts, TAlleleStats &stats)
{
    //prepare a map to obtain the locus index from locus name in constant time
    std::unordered_map<std::string, unsigned> locusNameToIndex(2*length(locusNames)); //std::strings for hashing
    for(size_t i = 0; i < length(locusNames); ++i)
        locusNameToIndex[toCString(locusNames[i])] = i;

    //pepare the allele stats:
    //  -set locus and allele id
    //  -prepare the profile matrix
    size_t numContigs  = length(contigStore);
    resize(stats, numContigs);
    for (size_t id = 0; id < length(contigStore); ++id)
    {
        SingleAlleleStats & allele = stats[id];
        //prepare profile matrix
        size_t contigLen = length(contigStore[id].seq);
        resize(allele.profile, 7); //counts for 'A','C','G','T','N','-' and insertion
        for (int i = 0; i < 7; ++i)
            resize(allele.profile[i], contigLen, 0);

        //get locusId and alleleId
        std::string contigName = toCString(names[id]);
        size_t delimPos = contigName.find_last_of(opts.delim);
        if (delimPos == std::string::npos)
        {
            std::cerr << "Invalid contig name! Expected delimiter \"" << opts.delim << "\" but not found! " << contigName << '\n';
            exit(1); //todo exception
        }
        std::string locusName(contigName.substr(0, delimPos));
        allele.locusId = locusNameToIndex[locusName];
        allele.alleleId = stoi(contigName.substr(delimPos+1));
    }
}


//compute the portion of unmapped read sequence overlapping the allele (not 3' or 5' overhang)
typedef decltype(BamAlignmentRecord::cigar) TCigarString;
typedef decltype(BamAlignmentRecordCore::beginPos) TBamAlignPos;
std::pair<TBamAlignPos,TBamAlignPos> getUnmatchedOverlaps(const TCigarString &cigarString, TBamAlignPos beginPos, TBamAlignPos endPos, size_t contigLength)
{
    TBamAlignPos leftUnmatched = 0;
    for (size_t i = 0; i < length(cigarString); ++i)
    {
        const char operation = cigarString[i].operation;
        if (operation == 'S' || operation == 'H')
            leftUnmatched += cigarString[i].count;
        else
            break;
    }
    TBamAlignPos rightUnmatched = 0;
    for (int i = length(cigarString) - 1; i >=0; --i)
    {
        const char operation = cigarString[i].operation;
        if (operation == 'S' || operation == 'H')
            rightUnmatched += cigarString[i].count;
        else
            break;
    }
    return std::make_pair(std::min(beginPos, leftUnmatched), std::min(static_cast<TBamAlignPos>(contigLength) - endPos, rightUnmatched));
}


//calculate the score for each allele according to the computed mapping profiles
//we use the same scoring as SRST2
void calcScores(TAlleleStats & stats, const TContigStore &contigStore, const Options & opts)
{
    const double errorProb = 0.01; //TODO: as param
    //get the maximum contig length
    size_t maxContigLen = 0;
    for (const auto & contig : contigStore)
    {
        maxContigLen = std::max(maxContigLen, length(contig.seq));
    }

    //prepare the expected pvalues and the match/mismatch counts, as we use the same container for all.
    String<double> expectedPvals, weightedPVals;
    resize(expectedPvals, 2 * maxContigLen); //will be large enough to hold also the insertion penalties!
    resize(weightedPVals, 2 * maxContigLen);

    String<unsigned> matches, nonMatches;
    resize(matches, maxContigLen);
    resize(nonMatches, maxContigLen);

    for (size_t idx = 0; idx < length(contigStore); ++idx)
    {
//        if (!(stats[idx].locusId == 6 && stats[idx].alleleId == "312"))
//            continue;
        //compute the number of matching and mismatching bases per position
        //mismatches also include indels
        const size_t contigLen = length(contigStore[idx].seq);

        //reset previous counts in matches/mismatches
        arrayFill(begin(matches), begin(matches)+contigLen, 0);
        arrayFill(begin(nonMatches), begin(nonMatches)+contigLen, 0);
        //get a const handle to the insertions for the allele
        const String<unsigned> & insertionsRef = stats[idx].profile[6];

        //the maximum depth of all bases for that allele (match/mismatch + insertions!)
        unsigned maxDepth = 0;
        unsigned summedDepth = 0;
        unsigned depth5Prime = 0;
        unsigned depth3Prime = 0;
        unsigned insertionsTotal = 0;
        unsigned deletionsTotal = 0;
        unsigned mismatchesTotal = 0;
        unsigned coveredPos = 0;

        for (size_t pos = 0; pos < contigLen; ++pos)
        {
            size_t refBase = static_cast<Dna5>(contigStore[idx].seq[pos]).value;
            matches[pos] = stats[idx].profile[refBase][pos];
            unsigned deletionsPos = stats[idx].profile[5][pos];
            unsigned mismatchesPos = 0;
            for (size_t i = 0; i < 5; ++i)
            {
                if (i != refBase)
                    mismatchesPos += stats[idx].profile[i][pos];
            }

            const unsigned depth = matches[pos] + nonMatches[pos];
            if (depth)
            {
                maxDepth = std::max(maxDepth, depth);
                ++coveredPos;
            }

            nonMatches[pos] = mismatchesPos + deletionsPos;

            if (deletionsPos > matches[pos])
                ++deletionsTotal;
            if (nonMatches[pos] > matches[pos])
                ++mismatchesTotal;
            if(insertionsRef[pos] > depth/2.)
                ++insertionsTotal;

            if (pos < opts.terminalLength)
                depth5Prime += depth;
            if (pos >= contigLen - opts.terminalLength)
                depth3Prime += depth;

            summedDepth += depth;
        }

        stats[idx].numInsertions = insertionsTotal;
        stats[idx].numDeletions = deletionsTotal;
        stats[idx].mismatches = mismatchesTotal;
        stats[idx].divergence = static_cast<double>(mismatchesTotal + deletionsTotal) / coveredPos;
        stats[idx].avgDepth = static_cast<double>(summedDepth) / coveredPos;
        stats[idx].avgDepth5Prime = static_cast<double>(depth5Prime) / opts.terminalLength;
        stats[idx].avgDepth3Prime = static_cast<double>(depth3Prime) / opts.terminalLength;
        stats[idx].coverage = static_cast<double>(coveredPos) / contigLen;

        //check for truncation or internal deletions (i.e. uncovered regions)
        size_t lastCoveredPos = -1;
        for (size_t pos = 0; pos < contigLen; ++pos)
        {
            //compute depth
            //skip positions that are uncovered. For each uncovered interval [i,j] we will compute the depth as:
            //  1. avg of positions i-1, j+1 (store at positon i) -- internal region
            //  2. avg of positions j+1, j+2 (store at position j) -- 5' truncation
            //  3. avg of positions i-2, i-1 (store at position i) -- 3' truncation
            const unsigned depthPos = nonMatches[pos] + matches[pos];
            if (depthPos == 0) //skip uncovered positions
            {
                continue;
            }
            else
            {
                //check for end of uncovered region
                if(lastCoveredPos != pos-1) //we just reached the end of an uncovered region.
                {
                    if (lastCoveredPos == -1) // 5' truncation (CASE 2)
                    {
                        if(pos < contigLen - 1)
                            nonMatches[pos-1] = (depthPos + matches[pos+1] + nonMatches[pos+1]) / 2.;
                        else
                            nonMatches[pos-1] = nonMatches[pos];

                        lastCoveredPos = pos;
#ifdef DEBUG
                        std::cerr << pos << " " << "5' truncation " << nonMatches[pos-1] << "\n";
#endif
                    }
                    else //internal deletion (CASE 1)
                    {
#ifdef SRST2
                        //take avg of two bases in the right (why?)
                        nonMatches[pos-1] = (matches[pos+1] + nonMatches[pos+1] + depthPos) / 2.;
#else
                        //take average to left and right base
                        nonMatches[pos-1] = 0.5 + (matches[lastCoveredPos] + nonMatches[lastCoveredPos] + depthPos) / 2.;
#endif
                        lastCoveredPos = pos;
#ifdef DEBUG
                        std::cerr << pos << " " <<  "internal deletion " << nonMatches[pos-1] << '\n';
#endif
                    }
                }
                lastCoveredPos = pos;
#ifdef DEBUG
                if (insertionsRef[pos])
                    std::cerr << pos+1 << " insertion " << " " << insertionsRef[pos] << '\n';
                std::cerr << pos+1 << " match " << " " << matches[pos] << " " << nonMatches[pos] << '\n';
#endif
            }
        }
        //check for 3' truncation (CASE 3)
        if (lastCoveredPos != contigLen - 1) // 3' end truncation
        {
            if (lastCoveredPos != -1)
            {
                if(lastCoveredPos > 0)
                    nonMatches[contigLen - 1] = (matches[lastCoveredPos] + nonMatches[lastCoveredPos] + matches[lastCoveredPos - 1] + nonMatches[lastCoveredPos - 1] )/ 2.;
                else
                    nonMatches[contigLen - 1] = matches[lastCoveredPos] + nonMatches[lastCoveredPos];
#ifdef DEBUG
                std::cerr << contigLen << " " << "3' truncation " << nonMatches[contigLen - 1] << '\n';
#endif
            }
            else
            {
#ifdef DEBUG
                std::cerr << "no aligned bases -- skipping allele \n";
#endif
                continue;
            }
        }

        //get weighted pvalues (weighted by relative base coverage)
        size_t numPVals = 0;
        for (size_t i = 0; i < contigLen; ++i)
        {
            const unsigned depth = matches[i] + nonMatches[i];
            //binomial distribution for score calculation
            if (depth > 0)
            {
                boost::math::binomial bino(depth, 1.0 - errorProb);
                double pVal = boost::math::cdf(bino, matches[i]);
                //std::cout << i << '\t' << pVal << '\t' << static_cast<double>(depth) / maxDepth << '\n';
                weightedPVals[numPVals++] = -log10(pVal * static_cast<double>(depth) / maxDepth);
//                std::cout << weightedPVals[numPVals-1] << ' ' << matches[i] + mismatches[i] << ' ' << pVal << ' ' << maxCov << '\n';
            }
            //handle insertions
            if (insertionsRef[i] > 0)
            {
                const unsigned covWithInserts = depth;// + insertionsRef[i];
                boost::math::binomial bino(covWithInserts, 1.0 - errorProb);
                double pVal = boost::math::cdf(bino, insertionsRef[i]);
                //std::cout << i << '\t' << pVal << '\t' << static_cast<double>(covWithInserts) / maxDepth << '\n';
                weightedPVals[numPVals++] = -log10(pVal * static_cast<double>(covWithInserts) / maxDepth);
            }
        }

        //generate expected distribution in the predefined container
        unsigned generatorIndex = numPVals;
        std::generate_n(begin(expectedPvals), numPVals, [&generatorIndex, numPVals](){return -log10(generatorIndex--/static_cast<double>(numPVals+1));});

        //sort the log transformed values and compare against expected distribution
        std::sort(begin(weightedPVals), begin(weightedPVals) + numPVals);
#ifdef DEBUG
        for (size_t i = 0; i < numPVals; ++i)
            std::cout << expectedPvals[i] << ' ';
        std::cout << '\n';
        for (size_t i = 0; i < numPVals; ++i)
            std::cout << weightedPVals[i] << ' ';
        std::cout << '\n';
#endif

        std::pair<double, double> slopeIntercept = computeRegressionN(expectedPvals, weightedPVals, numPVals);
//        for(size_t kk = 0; kk < numPVals; ++kk)
//            std::cout << expectedPvals[kk] << " vs " << weightedPVals[kk] << '\n';
#ifdef DEBUG
        std::cout << "Num Pvals: " << numPVals << " slope: " << slopeIntercept.first << '\n';
#endif
        stats[idx].score = slopeIntercept.first;
    }
}


void calcAllelStats(TAlleleStats & alleleStats, BamFileIn &bamfile, const TContigStore &contigStore, const Options &opts)
{
    //parse the bam file and adjust the profiles for the corresponding allele
    BamHeader header;
    readHeader(header, bamfile);
    BamAlignmentRecord record;

    while(!atEnd(bamfile))
    {
        readRecord(record, bamfile);

        if (record.beginPos == record.INVALID_POS) //skip unaligned read
            continue;
        if (!hasFlagAllProper(record)) //skip non properly aligned reads/pairs
            continue;
        if(record.mapQ < opts.minMappingScore) //skip low scoring alignment
            continue;

        SingleAlleleStats::TProfile & profile = alleleStats[record.rID].profile;

        //parse the cigar string
        //get endpos of read for calculation of unmatched overlaps
        size_t readPos = 0;
        size_t contigPos = record.beginPos;
        for (const auto & cigarElem : record.cigar)
        {
            if(cigarElem.operation == 'M' || cigarElem.operation == '=' || cigarElem.operation == 'X') //count (mis)matched character
            {
                readPos += cigarElem.count;
                contigPos += cigarElem.count;
            }
            else if (cigarElem.operation == 'D' || cigarElem.operation == 'N') //count gap character
            {
                contigPos += cigarElem.count;
            }
            else if(cigarElem.operation == 'I' || cigarElem.operation == 'S') //skip read positions
            {
                readPos += cigarElem.count;
            }
        }
        std::pair<unsigned,unsigned> unmatchedOverlaps = getUnmatchedOverlaps(record.cigar, record.beginPos, contigPos, length(contigStore[record.rID].seq));

        if(unmatchedOverlaps.first > opts.maxUnmatchedOverlaps  || unmatchedOverlaps.second > opts.maxUnmatchedOverlaps)
        {
            //std::cerr << "Skipping read due to large overlaps!\n";
            continue;
            //todo write to logfile
        }

        //now parse cigar once again to fill the profiles
        readPos = 0;
        contigPos = record.beginPos;
        for (const auto & cigarElem : record.cigar)
        {
            if(cigarElem.operation == 'M' || cigarElem.operation == '=' || cigarElem.operation == 'X') //count (mis)matched character
            {
                for (int i = 0; i < cigarElem.count; ++i, ++readPos, ++contigPos)
                {                    
                    if(record.qual[readPos] >= opts.minPhredScore + 33)
                        ++profile[static_cast<Dna5>(record.seq[readPos]).value][contigPos];
#ifdef DEBUG
                    else
                        std::cerr << "skipping due to low Phred score " << contigPos << '\n';
#endif
                }
            }
            else if (cigarElem.operation == 'D' || cigarElem.operation == 'N') //count gap character
            {
                for (int i = 0; i < cigarElem.count; ++i, ++contigPos)
                    ++profile[5][contigPos];                
            }
            else if(cigarElem.operation == 'I') //add single penalty for insertion
            {
                for (int i = 0; i < cigarElem.count; ++i)
                {
                    if(record.qual[readPos + i] >= opts.minPhredScore + 33)
                    {
                        ++profile[6][contigPos];
#ifdef DEBUG
                        std::cout << "Insertion at: " << contigPos << '\n';
#endif
                        break;
                    }
                }
                readPos += cigarElem.count;
            }
            else if(cigarElem.operation == 'S') //skip read positions
            {
                readPos += cigarElem.count;
            }
        }
    }

    //calculate score
    calcScores(alleleStats, contigStore, opts);
}


enum TIssues
{
    LOW_DEPTH,
    LOW_DEPTH_3PRIME,
    LOW_DEPTH_5PRIME,
    NUM_ISSUES
};
typedef std::bitset<NUM_ISSUES +1> TIssueMask;
//typedef Pair<unsigned, TIssueMask> TIdIssuePair;

void getTopLocusAlleles(String<unsigned> & bestAlleles, String<TIssueMask> & issues, const String<CharString> & locusNames, const TAlleleStats & stats, const Options & opts)
{
    //contains for each locus the id of the best candidate and a mask encoding issues to be reported
    resize(bestAlleles, length(locusNames), -1);
    resize(issues, length(locusNames));

    //group alleles by locus to obtain the best hit for each locus
    String<String<size_t>> allelesByLocus;
    resize(allelesByLocus, length(locusNames));
    for (size_t i = 0; i < length(stats); ++i)
    {
        //consider only candidates with sufficient coverage
        if (stats[i].coverage >= opts.minCoverage)
            append(allelesByLocus[stats[i].locusId], i);
    }

    //Identify top scoring allele for each locus and check for depth issues
    for (size_t locusId = 0; locusId < length(locusNames); ++locusId)
    {
        auto &locusAlleles = allelesByLocus[locusId];
        if (empty(locusAlleles))
        {
            continue;
        }
        //sort for each locus according to score
        std::sort(begin(locusAlleles), end(locusAlleles), [&stats](size_t left, size_t right){return stats[left].score < stats[right].score;});

        size_t bestIndex = 0;
        const double bestScore = stats[locusAlleles[0]].score;
        TIssueMask bestIssues, currentIssues;
        bestIssues.set();
        for (size_t idx = 0; idx < length(locusAlleles); ++idx)
        {
            const SingleAlleleStats & candidateAllele = stats[locusAlleles[idx]];

            //consider only optimal candidates (of multiple alleles with the same score exist)
            if (candidateAllele.score != bestScore)
                break;

            currentIssues.reset(); //delete previous issues
            if (candidateAllele.avgDepth < opts.minAvgDepth)
                currentIssues.set(LOW_DEPTH);
            else if (candidateAllele.avgDepth3Prime < opts.minTerminalDepth)
                currentIssues.set(LOW_DEPTH_3PRIME);
            else if (candidateAllele.avgDepth5Prime < opts.minTerminalDepth)
                currentIssues.set(LOW_DEPTH_5PRIME);

            //test for candidate with less issues
            if(currentIssues.count() < bestIssues.count())
            {
                bestIndex = idx;
                bestIssues = currentIssues;
            }
        }
        //set the best scoring candidate with least issues
        bestAlleles[locusId] = locusAlleles[bestIndex];
        issues[locusId] = bestIssues;
    }
}

void writeOutput(const String<unsigned> & bestAlleles,
                 const String<TIssueMask> & issues,
                 const String<CharString> & locusNameStore,
                 const TAlleleStats & alleleStats,
                 const TSequenceTypeList & seqTypes,
                 const Options & opts)
{

    //------------------------------------
    //-----------SUMMARY OUTPUT-----------
    //------------------------------------

    String<String<CharString>> csv;
    resize(csv,9);
    const CharString row_names [] = {"Loci", "Alleles", "Issues", "Coverage", "Avg. Depth", "#Mismatches", "#Insertions", "#Deletions", "Divergence"};
    for(size_t l = 0; l < length(csv); ++l)
        appendValue(csv[l],row_names[l]);

    for(size_t i = 0; i < length(locusNameStore); ++i)
    {
        const SingleAlleleStats & currentAllele = alleleStats[bestAlleles[i]];
         //Locus Name
        appendValue(csv[0], locusNameStore[i]);
        //Allele id
        if (bestAlleles[i] != -1)
        {
            appendValue(csv[1], std::to_string(currentAllele.alleleId));
            //Issues
            StringSet<CharString> issuesTmp;
            if (issues[i].test(LOW_DEPTH))
                appendValue(issuesTmp, "low depth");
            if (issues[i].test(LOW_DEPTH_3PRIME))
                appendValue(issuesTmp, "low depth 3'");
            if (issues[i].test(LOW_DEPTH_5PRIME))
                appendValue(issuesTmp, "low depth 5'");
            if (!empty(issuesTmp))
                appendValue(csv[2], concat(issuesTmp,','));
            else
                appendValue(csv[2],"-");
            //stats
            appendValue(csv[3], std::to_string(currentAllele.coverage));
            appendValue(csv[4], std::to_string(currentAllele.avgDepth));
            appendValue(csv[5], std::to_string(currentAllele.mismatches));
            appendValue(csv[6], std::to_string(currentAllele.numInsertions));
            appendValue(csv[7], std::to_string(currentAllele.numDeletions));
            appendValue(csv[8], std::to_string(currentAllele.numDeletions));
        }
        else //if no allele found simple add a '-'
        {
            for(size_t r = 1; r < 9; ++r)
                appendValue(csv[r], "-");
        }
    }

    //Known sequence type?
    appendValue(csv[0], "ST");
    unsigned st = -1;

    String<unsigned> bestAlleleIds; //convert contig ids to allele ids
    for(const auto a : bestAlleles)
        appendValue(bestAlleleIds, alleleStats[a].alleleId);
    auto it = std::find(begin(seqTypes.sequenceTypes), end(seqTypes.sequenceTypes), bestAlleleIds);
    if (it != end(seqTypes.sequenceTypes))
        st = it - begin(seqTypes.sequenceTypes);

    if(st != -1)
        appendValue(csv[1], std::to_string(st));
    else
        appendValue(csv[1], "unknown");

    //write to outfile
    CharString sep = ';';

    std::ostringstream ss;
    for (const auto & line : csv)
        ss << concat(line, sep) << '\n';

    bool writeSuccess = false;
    if (!empty(opts.outFileName))
    {
        std::ofstream of(toCString(opts.outFileName));
        if(of.is_open())
        {
            of << ss.str();
            of.close();
            writeSuccess = true;
        }
        else
        {
            std::cerr << "error opening outfile for writing: " << opts.outFileName << '\n';
            std::cerr << "writing results to stdout!\n";
        }
    }
    if (!writeSuccess) //if no outfile given or opening failed write to command line
        std::cout << ss.str();

    //------------------------------------
    //-----------DETAILED OUTPUT----------
    //------------------------------------

    std::ofstream ofCov("cov.txt");
    //per position per base count for construction of nice coverage plots
    for(size_t i = 0; i < length(locusNameStore); ++i)
    {
        const SingleAlleleStats & currentAllele = alleleStats[bestAlleles[i]];
        ofCov << locusNameStore[i] << " " << std::to_string(currentAllele.alleleId) << '\n';
        for (size_t i = 0; i < length(currentAllele.profile); ++i)
            ofCov << concat(currentAllele.profile[i], ',') << '\n';
    }
    ofCov.close();


}


seqan::ArgumentParser::ParseResult
parseCommandLine(Options & options, int argc, char const ** argv)
{
    // Setup ArgumentParser.
    seqan::ArgumentParser parser("mlstTool");
    seqan::setVersion(parser, "0.1");
    seqan::setDate(parser, "August 2017");

    //alleles fasta file
    seqan::addOption(parser, seqan::ArgParseOption(
                         "a", "input-alleles", "Path to the input file with allele sequences",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "input-alleles", "fa fasta");
    setRequired(parser, "a");
    //mapped reads sam/bam
    seqan::addOption(parser, seqan::ArgParseOption(
                         "r", "input-reads", "Path to the input file with aligned reads",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "input-reads", "bam sam");
    setRequired(parser, "r");

    //sequence types csv file
    seqan::addOption(parser, seqan::ArgParseOption(
                         "s", "sequence-types", "Path to the input file with known sequence types",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "sequence-types", "csv txt");

    //output file
    seqan::addOption(parser, seqan::ArgParseOption(
                         "o", "output-file", "Path to the output file - if not given result will be printed to stdout",
                         seqan::ArgParseArgument::OUTPUT_FILE, "OUT"));

    //Options
    seqan::addOption(parser, seqan::ArgParseOption(
                         "d", "contig-delimiter", "Delimiter character for alleles (<locus><delim><allele number>)",
                         seqan::ArgParseArgument::STRING, "CHAR"));
    setDefaultValue(parser, "contig-delimiter", "-");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "ms", "min-mapping-score", "minimal mapping score for aligned reads",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-mapping-score", 1);
    seqan::addOption(parser, seqan::ArgParseOption(
                         "bs", "min-base-score", "minimal Phred score for aligned bases",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-base-score", 20);
    setMinValue(parser, "min-base-score", "0");
    setMaxValue(parser, "min-base-score", "60");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "ad", "min-average-depth", "minimal average read depth for an allele (otherwise a warning will be reported)",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    seqan::addOption(parser, seqan::ArgParseOption(
                         "uo", "max-unaligned-overlap", "maximal length of clipped bases overlapping the allele (not at the 5' or 3' end)",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "max-unaligned-overlap", 20);

    setDefaultValue(parser, "min-average-depth", 2);
    seqan::addOption(parser, seqan::ArgParseOption(
                         "mc", "min-coverage", "minimal fraction of an allele to be covered by read alignments",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "min-coverage", 0.9);
    setMinValue(parser, "min-coverage", "0.1");
    setMaxValue(parser, "min-coverage", "1.0");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "tl", "terminal-length", "length of the terminal regions (5' and 3') of the allele. Important for option \"min-terminal-depth\"",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "terminal-length", 2);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "td", "min-terminal-depth", "minimal depth required for the terminal regions of the allele (otherwise a warning will be reported)",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-terminal-depth", 2);



    // Parse command line.
    seqan::ArgumentParser::ParseResult res = seqan::parse(parser, argc, argv);

    // Only extract  options if the program will continue after parseCommandLine()
    if (res != seqan::ArgumentParser::PARSE_OK)
        return res;

    // Extract option values.
    std::string delim;
    getOptionValue(delim, parser, "contig-delimiter");
    if (delim.length() > 1)
    {
        std::cerr << "INVALID DELIMITER! Must be single character but passed " << delim << '\n';
        exit(1);
    }
    options.delim = delim[0];

    getOptionValue(options.maxUnmatchedOverlaps, parser, "max-unaligned-overlap");
    getOptionValue(options.terminalLength, parser, "terminal-length");
    getOptionValue(options.minTerminalDepth, parser, "min-terminal-depth");
    getOptionValue(options.minAvgDepth, parser, "min-average-depth");
    getOptionValue(options.minCoverage, parser, "min-coverage");
    unsigned tmpMinPhredScore;
    getOptionValue(tmpMinPhredScore, parser, "min-base-score");
    options.minPhredScore = tmpMinPhredScore;
    getOptionValue(options.minMappingScore, parser, "min-mapping-score");

    seqan::getOptionValue(options.bamFileName, parser, "input-reads");

    seqan::getOptionValue(options.alleleFileName, parser, "input-alleles");
    if (isSet(parser, "sequence-types"))
        seqan::getOptionValue(options.stFileName, parser, "sequence-types");
    if (isSet(parser, "output-file"))
        seqan::getOptionValue(options.outFileName, parser, "output-file");

    return seqan::ArgumentParser::PARSE_OK;
}

int main(int argc, const char ** argv)
{
    //-------------------------------
    //-----HANDLING COMMMAND LINE----
    //-------------------------------
    Options opts;
    seqan::ArgumentParser::ParseResult res = parseCommandLine(opts, argc, argv);

    // If parsing was not successful then exit with code 1 if there were errors.
    // Otherwise, exit with code 0 (e.g. help was printed).
    if (res != seqan::ArgumentParser::PARSE_OK)
        return res == seqan::ArgumentParser::PARSE_ERROR;

    //--------------------------------
    //--------------READ INPUT--------
    //--------------------------------

    BamFileIn bamFileIn;
    TFragmentStore fragStore;
    TSequenceTypeList seqTypes;
    String<CharString> locusNameStore;
    loadData(fragStore, bamFileIn, seqTypes, locusNameStore, opts);

    //--------------------------------
    //-----------ANALYSIS-------------
    //--------------------------------

    //setup data structures holding the stats for each allle and the names of the loci
    TAlleleStats alleleStats;

    setupAlleleStats(fragStore.contigStore, fragStore.contigNameStore, locusNameStore, opts, alleleStats);

    //calculate stats for each allele
    calcAllelStats(alleleStats, bamFileIn, fragStore.contigStore, opts);

#ifdef DEBUG
    for (int i = 0; i < length(alleleStats); ++i)
    {
        if (alleleStats[i].score  < 100)
            std::cout<< "avg cov " << fragStore.contigNameStore[i] << ": " << alleleStats[i].avgDepth << " Score: " << alleleStats[i].score << '\n';
    }
#endif

    //output
    String<unsigned> bestAlleles;
    String<TIssueMask> issues;
    getTopLocusAlleles(bestAlleles, issues, locusNameStore, alleleStats, opts);

    writeOutput(bestAlleles, issues, locusNameStore, alleleStats, seqTypes, opts);

//    String<unsigned> bestAlleleIds;
//    for(const auto a : bestAlleles)
//        appendValue(bestAlleleIds, alleleStats[a].alleleId);

//    //is this a known sequence type?
//    unsigned st = -1;
//    auto it = std::find(begin(seqTypes.sequenceTypes), end(seqTypes.sequenceTypes), bestAlleleIds);
//    if (it != end(seqTypes.sequenceTypes))
//        st = it - begin(seqTypes.sequenceTypes);

//    std::cout << "ST: ";
//    if(st != -1)
//        std::cout << st << '\n';
//    else
//        std::cout << "unknown\n";
//    for(size_t i = 0; i < length(locusNameStore); ++i)
//    {
//        std::cout << locusNameStore[i] << '\t';
//        if (bestAlleles[i] != -1)
//            std::cout << alleleStats[bestAlleles[i]].alleleId << '\t';
//        if (issues[i].test(LOW_DEPTH))
//            std::cout << "low depth ";
//        if (issues[i].test(LOW_DEPTH_3PRIME))
//            std::cout << "low depth 3' ";
//        if (issues[i].test(LOW_DEPTH_5PRIME))
//            std::cout << "low depth 5'";
//        std::cout <<'\n';
//    }

////    for (int i = 0; i < length(fragStore.contigStore[0].seq); ++i)
////    {
////        std::cout << fragStore.contigStore[0].seq[i] << ' ' << alleleStats[0].profile[0][i] << ' ' << alleleStats[0].profile[1][i] << ' ' << alleleStats[0].profile[2][i] << ' ' << alleleStats[0].profile[3][i] << ' ' << alleleStats[0].profile[4][i] << '\n';
////    }Insertion at:
    return 0;
}

