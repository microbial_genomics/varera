//    Copyright (c) 2017, Sandro Andreotti, MODAL AG
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//    3. Neither the name of the copyright holder nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
//    THE POSSIBILITY OF SUCH DAMAGE.
//
// ==========================================================================
// Author: Sandro Andreotti <sandro.andreotti@fu-berlin.de>
// ==========================================================================

#include <kmercounting.h>
#include <hybridcounting.h>
#include <mapcounting.h>
#include <reporting.h>

#include <contig_loading.h>

//std includes
#include <iostream>
#include <bitset>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>

//seqan includes
#include <seqan/basic.h>
#include <seqan/sequence.h>
#include <seqan/stream.h>
#include <seqan/bam_io.h>
#include <seqan/store.h>
#include <seqan/arg_parse.h>
#include <seqan/stream.h>
#include <seqan/index.h>


//#define DEBUG
#define SRST2

//@TODO: calculate and use edge depth for filtering -- for mlst

using namespace seqan;

typedef Dna5String TSeqType;
typedef String<SingleAlleleStats> TAlleleStats;
typedef String<SingleAlleleQStats> TAlleleQStats;
typedef String<unsigned> TAlleleCounts;

seqan::ArgumentParser::ParseResult
parseCommandLine(Options & options, int argc, char const ** argv)
{
    // Setup ArgumentParser.
    seqan::ArgumentParser parser("MLSTyper");
    seqan::setVersion(parser, "0.1");
    seqan::setDate(parser, "August 2017");

    //alleles fasta file
    seqan::addOption(parser, seqan::ArgParseOption(
                         "a", "input-alleles", "Path to the input file with allele sequences",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "input-alleles", "fa fasta");
    setRequired(parser, "a");

    //sequence types csv file
    seqan::addOption(parser, seqan::ArgParseOption(
                         "s", "sequence-types", "Path to the input file with known sequence types",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "sequence-types", "csv txt");

    //output file
    seqan::addOption(parser, seqan::ArgParseOption(
                         "o", "output-file-summary", "Path to the output file with a summary of the identification run",
                         seqan::ArgParseArgument::OUTPUT_FILE, "OUT"));
    setValidValues(parser, "output-file-summary", "csv");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "afc", "allele-filter-cutoff", "Alleles deviating from the average length by more that this percentage are ignored. "
                                                       "Negative values deactivate filtering.",
                         seqan::ArgParseArgument::DOUBLE, "DOUBLE"));
    setDefaultValue(parser, "allele-filter-cutoff", 10);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "d", "contig-delimiter", "Delimiter character for alleles (<locus><delim><allele number>)",
                         seqan::ArgParseArgument::STRING, "CHAR"));
    setDefaultValue(parser, "contig-delimiter", "_");

    addSection(parser, "Kmer based identification");
    seqan::addOption(parser, seqan::ArgParseOption(
                         "r1", "input-reads1", "Path to the input file with raw reads. This turns on the kmer based mode! If not given \"input-reads-aligned\" must be provided!",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "input-reads1", SeqFileIn::getFileExtensions());
    seqan::addOption(parser, seqan::ArgParseOption(
                         "r2", "input-reads2", "Path to the input file with raw reads for paired-end data. This turns on the kmer based mode! If not given \"input-reads-aligned\" must be provided!",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "input-reads2", SeqFileIn::getFileExtensions());

    seqan::addOption(parser, seqan::ArgParseOption(
                         "k", "word-length", "Word length k",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "word-length", 35);
    setMinValue(parser, "word-length", "11");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "kcov", "kmer-coverage", "Gather coverage information in k-mer mode"));

    seqan::addOption(parser, seqan::ArgParseOption(
                         "mks", "min-kmer-support", "The minimal fraction of k-mers of the same read that must map to an allele in order to be counted",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "min-kmer-support", 0.5);
    seqan::addOption(parser, seqan::ArgParseOption(
                         "mkd", "min-kmer-depth", "The minimal average depth of k-mers (#matched kmers / length of allele). Otherwise a warning will be displayed",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "min-kmer-depth", 2.0);

    //mapped reads sam/bam
    addSection(parser, "Mapping based identification");
    seqan::addOption(parser, seqan::ArgParseOption(
                         "ra", "input-reads-aligned", "Path to the input file with aligned reads. This turns on the mapping based mode! If not given \"input-reads\" must be provided!",
                         seqan::ArgParseArgument::INPUT_FILE, "IN"));
    setValidValues(parser, "input-reads-aligned", "bam sam");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "c", "output-file-cov", "Path to the output file with coverage details used to create figures in report",
                         seqan::ArgParseArgument::OUTPUT_FILE, "OUT"));
    setValidValues(parser, "output-file-cov", "csv");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "ms", "min-mapping-score", "minimal mapping score for aligned reads",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-mapping-score", 1);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "bs", "min-base-score", "minimal Phred score for aligned bases",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-base-score", 20);
    setMinValue(parser, "min-base-score", "0");
    setMaxValue(parser, "min-base-score", "60");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "ad", "min-average-depth", "minimal average read depth for an allele (otherwise a warning will be reported)",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-average-depth", 5);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "adw", "min-average-depth-warn", "minimal average read depth for a gene (otherwise a warning will be reported)",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-average-depth-warn", 10);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "mc", "min-coverage", "minimal fraction of an allele to be covered by read alignments",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "min-coverage", 0.9);
    setMinValue(parser, "min-coverage", "0.1");
    setMaxValue(parser, "min-coverage", "1.0");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "mcw", "min-coverage-warn", "minimal fraction of a gene to be covered by read alignments",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "min-coverage-warn", 0.95);
    setMinValue(parser, "min-coverage-warn", "0.1");
    setMaxValue(parser, "min-coverage-warn", "1.0");

    seqan::addOption(parser, seqan::ArgParseOption(
                         "md", "max-divergence", "maximal allowed divergence for a gene to be regarded as a match",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "max-divergence", 0.02);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "mdw", "max-divergence-warn", "maximal allowed divergence for a gene to be regarded as a match",
                         seqan::ArgParseArgument::DOUBLE, "FLOAT"));
    setDefaultValue(parser, "max-divergence-warn", 0.01);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "uo", "max-unaligned-overlap", "maximal length of clipped bases overlapping the allele (not at the 5' or 3' end)",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "max-unaligned-overlap", 20);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "tl", "terminal-length", "length of the terminal regions (5' and 3') of the allele. Important for option \"min-terminal-depth\"",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "terminal-length", 2);

    seqan::addOption(parser, seqan::ArgParseOption(
                         "td", "min-terminal-depth", "minimal depth required for the terminal regions of the allele (otherwise a warning will be reported)",
                         seqan::ArgParseArgument::INTEGER, "INT"));
    setDefaultValue(parser, "min-terminal-depth", 2);


    // Parse command line.
    seqan::ArgumentParser::ParseResult res = seqan::parse(parser, argc, argv);

    // Only extract  options if the program will continue after parseCommandLine()
    if (res != seqan::ArgumentParser::PARSE_OK)
        return res;

    // Extract option values.
    std::string delim;
    getOptionValue(delim, parser, "contig-delimiter");
    if (delim.length() > 1)
    {
        std::cerr << "INVALID DELIMITER! Must be single character but passed " << delim << '\n';
        exit(1);
    }
    options.delim = delim[0];

    getOptionValue(options.alleleFilterCutoff, parser, "allele-filter-cutoff");
    getOptionValue(options.maxUnmatchedOverlaps, parser, "max-unaligned-overlap");
    getOptionValue(options.terminalLength, parser, "terminal-length");
    getOptionValue(options.minTerminalDepth, parser, "min-terminal-depth");

    getOptionValue(options.minAvgDepth, parser, "min-average-depth");
    getOptionValue(options.minAvgDepthW, parser, "min-average-depth-warn");
    getOptionValue(options.minCoverage, parser, "min-coverage");
    getOptionValue(options.minCoverageW, parser, "min-coverage-warn");
    getOptionValue(options.maxDivergence, parser, "max-divergence");
    getOptionValue(options.maxDivergenceW, parser, "max-divergence-warn");

    unsigned tmpMinPhredScore;
    getOptionValue(tmpMinPhredScore, parser, "min-base-score");
    options.minPhredScore = tmpMinPhredScore;
    getOptionValue(options.minMappingScore, parser, "min-mapping-score");
    getOptionValue(options.qLen, parser, "word-length");
    getOptionValue(options.qMinSupport, parser, "min-kmer-support");
    getOptionValue(options.qMinAvgDepth, parser, "min-kmer-depth");

    if(isSet(parser, "input-reads1") && isSet(parser, "input-reads-aligned"))
    {
        throw std::invalid_argument("Invalid call with aligned and raw reads! TERMINATE!");
    }
    else if(!isSet(parser, "input-reads1") && !isSet(parser, "input-reads-aligned"))
    {
        throw std::invalid_argument("Invalid call without aligned or raw reads! TERMINATE!");
    }
    else if(isSet(parser, "input-reads1"))
    {
        seqan::getOptionValue(options.readsFileName, parser, "input-reads1");
        if(isSet(parser, "input-reads2"))
            seqan::getOptionValue(options.readsFileName2, parser, "input-reads2");
        if (isSet(parser, "kmer-coverage"))
            options.mode = options.MLST_HYBRID;
        else
            options.mode = options.MLST_Q_GRAM;
    }
    else
    {
        seqan::getOptionValue(options.bamFileName, parser, "input-reads-aligned");
        options.mode = options.MLST_MAPPING;
    }

    seqan::getOptionValue(options.alleleFileName, parser, "input-alleles");
    if (isSet(parser, "sequence-types"))
        seqan::getOptionValue(options.stFileName, parser, "sequence-types");
    if (isSet(parser, "output-file-summary"))
        seqan::getOptionValue(options.summaryOutFileName, parser, "output-file-summary");
    if (isSet(parser, "output-file-cov"))
        seqan::getOptionValue(options.covOutFileName, parser, "output-file-cov");


    return seqan::ArgumentParser::PARSE_OK;
}

int main(int argc, const char ** argv)
{
    //-------------------------------
    //-----------TYPEDEFS------------
    //-------------------------------
    typedef Dna5String TContigSeq;
    typedef StringSet<TContigSeq> TContigSeqList;
//    typedef typename TFragmentStoreContigs::TContigSeq TContigSeq;
//    typedef StringSet<Infix<const TContigSeq>::Type> TContigSeqList;
    typedef Index<TContigSeqList, IndexQGram<SimpleShape, OpenAddressing>> TIndex;

    typedef Pair<unsigned, unsigned> TLocusAlleleId;

    //-------------------------------
    //-----HANDLING COMMMAND LINE----
    //-------------------------------
    Options opts;
    seqan::ArgumentParser::ParseResult res = parseCommandLine(opts, argc, argv);

    // If parsing was not successful then exit with code 1 if there were errors.
    // Otherwise, exit with code 0 (e.g. help was printed).
    if (res != seqan::ArgumentParser::PARSE_OK)
        return res == seqan::ArgumentParser::PARSE_ERROR;

    //--------------------------------
    //--------------READ INPUT--------
    //--------------------------------
    TFragmentStoreContigs fragStore;
    TSequenceTypeList seqTypes;
    String<CharString> locusNameStore;
    String<TLocusAlleleId> contigToLocus;
    loadContigData(fragStore, seqTypes, locusNameStore, contigToLocus, opts);

    if (opts.alleleFilterCutoff >= 0)
    {
        checkAlleles(contigToLocus, fragStore, locusNameStore, opts);
    }

    if(opts.mode == Options::MLST_MAPPING)
    {
        // Open Bamfile
        BamFileIn bamFileIn;
        if (!open(bamFileIn, toCString(opts.bamFileName)))
        {
            std::cerr << "ERROR: Could not open " << opts.bamFileName << std::endl;
            exit(1);
        }

        //--------------------------------
        //-----------ANALYSIS-------------
        //--------------------------------

        //setup data structures holding the stats for each allle and the names of the loci
        TAlleleStats bestAlleles;
        resize(bestAlleles, length(locusNameStore));

        //calculate stats for each allele
        calcAllelStats(bestAlleles, bamFileIn, fragStore.contigStore, contigToLocus, opts);

#ifdef DEBUG
        for (int i = 0; i < length(alleleStats); ++i)
        {
            if (alleleStats[i].score  < 100)
                std::cout<< "avg cov " << fragStore.contigNameStore[i] << ": " << alleleStats[i].avgDepth << " Score: " << alleleStats[i].score << '\n';
        }
#endif

        writeSummaryOutputMLST(bestAlleles, locusNameStore, seqTypes, opts);
        writeCoverageOutput(bestAlleles, fragStore.contigStore, fragStore.contigNameStore, opts);
    }

    if (opts.mode == Options::MLST_Q_GRAM)
    {
        //build q-gram index of contigs. Fist we have to extract the contigs from the fragment store via infix handles
        TContigSeqList contigSeqs;
        resize(contigSeqs, length(fragStore.contigStore));
        for(size_t i = 0; i < length(contigSeqs); ++i)
        {
            contigSeqs[i] = infix(fragStore.contigStore[i].seq, 0, length(fragStore.contigStore[i].seq));
        }
        TIndex qgramIndex(contigSeqs, opts.qLen);

        String<unsigned> contigCounts;
        resize(contigCounts, length(contigSeqs), 0);

        SeqFileIn readsFile(toCString(opts.readsFileName));
        kmerCounting(contigCounts, readsFile, qgramIndex, opts);
        close(readsFile);

        //in case of paired end simple add counts from second read file
        if(!empty(opts.readsFileName2))
        {
            open(readsFile, toCString(opts.readsFileName2));
            kmerCounting(contigCounts, readsFile, qgramIndex, opts);
            close(readsFile);
        }

        TAlleleQStats bestQAlleles;
        resize(bestQAlleles, length(locusNameStore));

        //find the best allele per locus
        for(size_t i = 0; i < length(contigSeqs); ++i)
        {
            if (contigCounts[i] > 0 && contigToLocus[i].i1 != static_cast<decltype(contigToLocus[i].i1)>(-1))
            {
                const auto & allele = contigToLocus[i];
                const double hitsPerBase = static_cast<double>(contigCounts[i]) / length(contigSeqs[i]);                
                if (hitsPerBase > bestQAlleles[allele.i1].numHitsPerBase)
                {   //update best allele for locus
                    bestQAlleles[allele.i1].contigId = i;
                    bestQAlleles[allele.i1].locusId = allele.i1;
                    bestQAlleles[allele.i1].alleleId = allele.i2;
                    bestQAlleles[allele.i1].numHits = contigCounts[i];
                    bestQAlleles[allele.i1].numHitsPerBase = hitsPerBase;
                    bestQAlleles[allele.i1].avgDepth = hitsPerBase;
                    bestQAlleles[allele.i1].issues.reset();

                    if(hitsPerBase < opts.qMinAvgDepth)
                        bestQAlleles[allele.i1].issues.set(SingleAlleleQStats::LOW_DEPTH);
                }
            }
        }
        writeSummaryOutputMLST(bestQAlleles, locusNameStore, seqTypes, opts);
    }
    if (opts.mode == Options::MLST_HYBRID)
    {
        //build q-gram index of contigs. Fist we have to extract the contigs from the fragment store via infix handles
        TContigSeqList contigSeqs;
        resize(contigSeqs, length(fragStore.contigStore));
        String<SingleAlleleHCounts> alleleCounts;
        resize(alleleCounts, length(fragStore.contigStore));
        for(size_t i = 0; i < length(contigSeqs); ++i)
        {
            contigSeqs[i] = infix(fragStore.contigStore[i].seq, 0, length(fragStore.contigStore[i].seq));
            resize(alleleCounts[i].profile, std::max(static_cast<int>(length(fragStore.contigStore[i].seq) - opts.qLen + 1),0),0.);
        }

        TIndex qgramIndex(contigSeqs, opts.qLen);
        SeqFileIn readsFile(toCString(opts.readsFileName));
        kmerHybridCounting(alleleCounts, readsFile, qgramIndex, opts);
        close(readsFile);

        //in case of paired end simply add counts from second read file
        if(!empty(opts.readsFileName2))
        {
            open(readsFile, toCString(opts.readsFileName2));
            kmerHybridCounting(alleleCounts, readsFile, qgramIndex, opts);
            close(readsFile);
        }

        //find the best allele per locus
        String<SingleAlleleHStats> bestHAlleles;
        resize(bestHAlleles, length(locusNameStore));

        for(size_t i = 0; i < length(contigSeqs); ++i)
        {
            if (alleleCounts[i].numHits > 0 && contigToLocus[i].i1 != static_cast<decltype(contigToLocus[i].i1)>(-1))
            {
                const auto & allele = contigToLocus[i];
                const double hitsPerBase = static_cast<double>(alleleCounts[i].numHits) / length(contigSeqs[i]);
                if (hitsPerBase > bestHAlleles[allele.i1].numHitsPerBase)
                {   //update best allele for locus
                    bestHAlleles[allele.i1].contigId = i;
                    bestHAlleles[allele.i1].locusId = allele.i1;
                    bestHAlleles[allele.i1].alleleId = allele.i2;
                    bestHAlleles[allele.i1].numHits = alleleCounts[i].numHits;
                    bestHAlleles[allele.i1].numHitsPerBase = hitsPerBase;
                    bestHAlleles[allele.i1].avgDepth = hitsPerBase;
                    bestHAlleles[allele.i1].issues.reset();
                    bestHAlleles[allele.i1].profile = alleleCounts[i].profile;

                    if(hitsPerBase < opts.qMinAvgDepth)
                        bestHAlleles[allele.i1].issues.set(SingleAlleleQStats::LOW_DEPTH);
                }
            }
        }
        writeSummaryOutputMLST(bestHAlleles, locusNameStore, seqTypes, opts);
        writeCoverageOutput(bestHAlleles, fragStore.contigNameStore, opts);
    }

    return 0;
}

