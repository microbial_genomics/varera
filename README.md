# VARERA


## Installation
* download/clone this git repository
* download the latest KNIME version from https://www.knime.com/downloads
* create a conda env with mlst_knime.yml and launch KNIME:
```
conda env  create -f mlst_knime.yml
source activate mlst_knime
./knime
```
* install the knime-package Knime4NGS as described in: http://ibisngs.github.io/knime4ngs/
* include the VARERA update-site into your KNIME as described in the previous step.
* import the workflows from the folder KNIME/Workflows
