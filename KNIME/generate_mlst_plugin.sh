#!/bin/bash

platform="mac_64"
plugin_dir="/Users/andreott/Work/MODAL/SEMMLER_Project/MLST/KNIME_NODE_GENERATION/mlst_plugin_sript"
build_dir="/Users/andreott/Work/MODAL/SEMMLER_Project/MLST/buildRelease"
knime_sdk=/Applications/KNIME SDK 3.3.2.app/

genericKnimeNodesRepo="git://github.com/genericworkflownodes/GenericKnimeNodes.git"
BSCKnimeNodesRepo="git@git.imp.fu-berlin.de:andreott/BSC-KNIME-nodes.git"

mkdir ${plugin_dir}/payload/bin
rm ${plugin_dir}/payload/bin/*
rm ${plugin_dir}/payload/*.zip

for binary in ${build_dir}/bin/*; do
	$binary -write-ctd ${plugin_dir}/descriptors/$(basename $binary).ctd
	cp $binary ${plugin_dir}/payload/bin/
done

zip -r ${plugin_dir}/payload/binaries_${platform}.zip ${plugin_dir}/payload/bin
rm -rf ${plugin_dir}/payload/bin

if [ -d "GenericKnimeNodes" ]; then
	rm -rf GenericKnimeNodes/generated_plugin
else
	git clone $genericKnimeNodesRepo
fi

if [ ! -d "BSC-KNIME-nodes" ]; then
	git clone $BSCKnimeNodesRepo
fi

bsc_checkout=$(pwd)"/BSC-KNIME-nodes"
echo $bsc_checkout

cd GenericKnimeNodes
echo ant -Dknime.sdk=${knime_sdk} -Dplugin.dir=${plugin_dir}
ant -Dknime.sdk=${knime_sdk} -Dplugin.dir=${plugin_dir}

ant -Dknime.sdk=${knime_sdk} -Dplugin.dir=${bsc_checkout}/trimmomatic
ant -Dknime.sdk=${knime_sdk} -Dplugin.dir=${bsc_checkout}/qualimap
